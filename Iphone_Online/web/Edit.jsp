

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "DAO.*" %>
<%@page import = "controller.*" %>
<%@page import = "java.util.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<!DOCTYPE html>
<%
ProductDAO x = new ProductDAO(); 
List<Product> lst= x.geProducts();
request.setAttribute("lst", lst);
%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            img{
                width: 200px;
                height: 120px;
            }
             .table-title{
                background-color: #52529b;
            }
            .modal-header h4{
                margin-left: 10%;
            }
        </style>
    <body>
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Edit <b>Product</b></h2>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div id="editEmployeeModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="edit" method="post">
                            <div class="modal-header">						
                                <h4 class="modal-title">Edit Product</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>                                                   
                            <div class="modal-body">	                                
                                <div class="form-group">
                                    <label>ID</label>
                                    <input value="${detail.getProductID()}" name="id" type="text" class="form-control" readonly required>
                                </div>     
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="${detail.getProductName()}" name="productName" type="text" class="form-control" required>
                                </div>
                                 <div class="form-group">
                                    <label>Color</label>
                                    <input value="${detail.getProductColor()}" name="producColor" type="text" class="form-control" required>
                                </div>   
                                <div class="form-group">
                                    <label>Image</label>
                                    <input value="${detail.getLink()}" name="link" type="images/" class="form-control" required>
                                </div>
                                  <div class="form-group">
                                    <label>Cost</label>
                                    <input value="${detail.getProductCost()}" name="productCost" type="number" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                  
                                    <input value="${detail.getProductPrice()}" name="productPrice" type="number" class="form-control" required>
                                </div>
                                 <div class="form-group">
                                    <label>Discount</label>
                                    <input value="${detail.getProductDiscount()}" name="productDiscount" type="number" class="form-control" required>
                                </div>  
                                 <div class="form-group">
                                    <label>Quantity</label>
                                    <input value="${detail.getProductQuantity()}" name="productQuantity" type="number" class="form-control" required>
                                </div>                                 
                            </div>                              
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Edit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>