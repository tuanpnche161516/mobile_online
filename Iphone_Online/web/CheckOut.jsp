<%-- 
    Document   : CheckOut
    Created on : Mar 14, 2023, 6:32:54 PM
    Author     : LEGION
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->

        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,800,900%7cRaleway:300,400,500,600,700" rel="stylesheet">
        <style>
            .row {
                display: -ms-flexbox; /* IE10 */
                display: flex;
                -ms-flex-wrap: wrap; /* IE10 */
                flex-wrap: wrap;
                margin: 0 -16px;
            }

            .col-25 {
                -ms-flex: 25%; /* IE10 */
                flex: 25%;
            }

            .col-50 {
                -ms-flex: 50%; /* IE10 */
                flex: 50%;
            }

            .col-75 {
                -ms-flex: 75%; /* IE10 */
                flex: 75%;
            }

            .col-25,
            .col-50,
            .col-75 {
                padding: 0 16px;
            }

            .container {
                background-color: #f2f2f2;
                padding: 5px 20px 15px 20px;
                border: 1px solid lightgrey;
                border-radius: 3px;
            }

            input[type=text] {
                width: 100%;
                margin-bottom: 20px;
                padding: 12px;
                border: 1px solid #ccc;
                border-radius: 3px;
            }

            label {
                margin-bottom: 10px;
                display: block;
            }

            .icon-container {
                margin-bottom: 20px;
                padding: 7px 0;
                font-size: 24px;
            }

            .btn {
                background-color: #04AA6D;
                color: white;
                padding: 12px;
                margin: 10px 0;
                border: none;
                width: 100%;
                border-radius: 3px;
                cursor: pointer;
                font-size: 17px;
            }

            .btn:hover {
                background-color: #45a049;
            }

            span.price {
                float: right;
                color: grey;
            }

            /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (and change the direction - make the "cart" column go on top) */
            @media (max-width: 800px) {
                .row {
                    flex-direction: column-reverse;
                }
                .col-25 {
                    margin-bottom: 20px;
                }
            }
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #f5f5f5;
            }

            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }

            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }

        </style>
    </head>
    <body>
        <c:set var="o" value="${requestScope.cart}"/>
        <header class="header">               
            <a href="" class="logo">Apple Luxury</a>
            <nav class="nav-items">
                <a href="index.jsp">Home</a>
                <a href="Product.jsp">Product</a>
                <a href="Contact.jsp">Contact</a>
            </nav>
        </header>
        <div class="row">
            <div class="col-md-10">
                <div class="container">
                    <form action="checkout" method="post">

                        <div class="row">
                            <div class="col-50">
                                <h3>Billing Address</h3>
                                <label for="fname"><i class="fa fa-user"></i> Full Name</label>
                                <input type="text" id="name" name="name" placeholder="">
                                <label for="email"><i class="fa fa-envelope"></i> Email</label>
                                <input type="text" id="email" name="email" placeholder="">
                                <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                                <input type="text" id="adr" name="adr" placeholder="">
                                 <label for="phone"><i class="fa fa-address-card-o"></i> Phone Number</label>
                                <input type="text" id="phone" name="phone" placeholder="">
                                <label for="city"><i class="fa fa-institution"></i> City</label>
                                <input type="text" id="city" name="city" placeholder="">  
                                <label for="city"><i class="fa fa-user"></i> Masages</label>
                                <input type="text" id="mass" name="mass" placeholder="">  
                            </div>
                        </div>
                        <label>
                            <input type="checkbox" checked="checked" name="sameadr"> Shipping address same as billing
                        </label>
                        <input type="submit" value="Continue to checkout" onclick="my_submit()" class="btn">
                    </form>
                </div>
            </div>           
            <div class="col-md-2">
                <div class="container">
                    <p>Total <span class="price" style="color:black"><b>$ ${o.totalMoney+2}</b></span></p>
                </div>
            </div>       
        </div>
                 <script>
            function my_submit() {
                var exampleInputName = document.getElementById("name");
                var exampleInputEmail = document.getElementById("email");
                var exampleInputadr = document.getElementById("adr");
                var exampleInputcity = document.getElementById("city");
                  var exampleInputPhone = document.getElementById("phone");
                   var exampleInputMass = document.getElementById("mass");
                   
                  var phone=exampleInputPhone.value;
                var name = exampleInputName.value;
                var email = exampleInputEmail.value;
                var adr = exampleInputadr.value;
                var city = exampleInputcity.value;
                 var mass = exampleInputMass.value;
                if (email.length == 0 || name.length == 0 || adr.length==0 || city.length==0 || phone.length==0 || mass.length==0) {
                    if (email.length == 0) {
                        alert("Bạn chưa nhập Email");
                        return false;
                    }
                    if (name.length == 0) {
                        alert("Bạn chưa nhập Name");
                        return false;
                    }
                    if (adr.length == 0) {
                        alert("Bạn chưa nhập Address");
                        return false;
                    }
                    if (phone.length == 0) {
                        alert("Bạn chưa nhập Phone");
                        return false;
                    }
                    if (city.length == 0) {
                        alert("Bạn chưa nhập City");
                        return false;
                    }
                    if (mass.length == 0) {
                        alert("Bạn chưa nhập Massage");
                        return false;
                    }
                } else {
                    var form = document.getElementById("my_form");
                    form.submit();
                    alert("Đặt hàng thành công");
                }
            }
        </script>

    </body>
</html>
