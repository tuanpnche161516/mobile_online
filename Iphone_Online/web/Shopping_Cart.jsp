

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
             .no-cart{
             margin-right: 5%;  
            }
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #f5f5f5;
            }

            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }
            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }
                  
            .table-responsive{
                background: gray;
            }
            .pb-5{
               background: gray;
            }
            .btnSub{
                background: green;
            }
            .btnAdd{
                background: #DC3545;
            }
            
            
        </style>
    </head>

    <body>
        <c:set var="o" value="${requestScope.cart}"/>
        <div class="header">
            <a href="Product.jsp" class="logo">Apple Luxury</a>
            <nav class="nav-items">
                <a href="index.jsp">Home</a>
                <a href="Product.jsp">Product</a>
                <a href="Contact.jsp">Contact</a>
            </nav>           
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
           <a class="btn btn-success btn-sm ml-3" href="">
                    <i class="fa fa-shopping-cart"></i> Cart
                    <span class="badge badge-light"></span>
                </a>
        </div>        
            <div class="shopping-cart">
                <div class="px-4 px-lg-0">

                    <div class="pb-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

                                    <!-- Shopping cart table -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            
                                            <thead class="checkout">
                                                <tr>                                   
                                                   <th scope="col" class="border-0 bg-light">
                                                        <div class="p-2 px-3 text-uppercase">Sản Phẩm</div>
                                                    </th>
                                                    <th scope="col" class="border-0 bg-light">
                                                        <div class="p-2 px-3 text-uppercase">Hình Ảnh</div>
                                                    </th>
                                                    <th scope="col" class="border-0 bg-light">
                                                        <div class="py-2 text-uppercase">Đơn Giá</div>
                                                    </th>
                                                    <th scope="col" class="border-0 bg-light">
                                                        <div class="py-2 text-uppercase">Số Lượng</div>
                                                    </th>
                                                    <th scope="col" class="border-0 bg-light">
                                                        <div class="py-2 text-uppercase">Xóa</div>
                                                    </th>
                                                </tr>                                    
                                            </thead>
                                           
                                            <tbody>
                                           
                                                <c:forEach items="${o.items}" var="i">
                                                <tr>
                                                    <th scope="row">
                                                        <div class="p-2">
                                                            <img src="" alt="" width="70" class="img-fluid rounded shadow-sm">
                                                            <div class="ml-3 d-inline-block align-middle">
                                                                <h5 class="mb-0"> <a href="#" class="text-dark d-inline-block"></a>${i.product.productName}</h5><span class="text-muted font-weight-normal font-italic"></span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td class="align-middle"><strong></strong></td>
                                                    <td class="align-middle"><strong>$ ${i.product.productPrice}</strong></td>
                                                    <td class="align-middle">
                                                        <a href="sub?quantity=-1&id=${i.product.productID}"><button class="btnSub">-</button></a> 
                                                        <strong>${i.quantity}</strong>
                                                        <a href="sub?quantity=1&id=${i.product.productID}"><button class="btnAdd">+</button></a>
                                                    </td>
                                              
                                                    <td class="align-middle"><a href="#" class="text-dark">
                                                            <input type="hidden" name="id" value="${i.product.productID}"/>
                                                            <a href="del?id=${i.product.productID}" class="btn btn-danger">Delete</a>
                                                        </a>
                                                    </td>
                                                </tr> 
                                            </c:forEach>  
                                         
                                        </tbody>
                                        </thead>
                                    </table>
                                </div>
                                <!-- End -->
                            </div>
                        </div>

                        <div class="row py-5 p-4 bg-white rounded shadow-sm">                         
                            <div class="col-lg-6">
                                <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Thành tiền</div>
                                <div class="p-4">
                                    <form action="checkout" method="post">
                                    <ul class="list-unstyled mb-4">                            
                                        <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Tổng tiền hàng</strong><strong>$ ${o.totalMoney}</strong></li>                                     
                                        <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Phí vận chuyển</strong><strong>$ 2</strong></li>
                                        <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Tổng thanh toán</strong>
                                            <h5 class="font-weight-bold">$ ${o.totalMoney+2}</h5>
                                        </li>                                        
                                    </ul><a href="Successfull.jsp" class="btn btn-dark rounded-pill py-2 btn-block">Mua hàng</a>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </body>

</html>

