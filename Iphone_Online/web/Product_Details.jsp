

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "DAO.*" %>
<%@page import = "controller.*" %>
<%@page import = "java.util.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%
ProductDAO x = new ProductDAO(); 
List<Product> lst= x.geProducts();
request.setAttribute("lst", lst);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>

            .card {
                margin-bottom: 30px;
            }
            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid transparent;
                border-radius: 0;
            }
            .card .card-subtitle {
                font-weight: 300;
                margin-bottom: 10px;
                color: #8898aa;
            }
            .table-product.table-striped tbody tr:nth-of-type(odd) {
                background-color: #f3f8fa!important
            }
            .table-product td{
                border-top: 0px solid #dee2e6 !important;
                color: #728299!important;
            }
            .mt-5{
                color: red;
            }
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #f5f5f5;
            }

            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }

            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }
            .no-cart{
                margin-right: 5%;
            }
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #f5f5f5;
            }

            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }
            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }
        </style>

    </head>
    <body>
        <div class="header">
            <a href="Product.jsp" class="logo">Apple Luxury</a>
            <nav class="nav-items">
                <a href="index.html">Home</a>
                <a href="Product.jsp">Product</a>
                <a href="Contact.jsp">Contact</a>
                <a href="ManagerProduct.jsp">Manager Product</a>
            </nav>         
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <a class="btn btn-success btn-sm ml-3" href="gui">
                <i class="fa fa-shopping-cart"></i> Cart
                <span class="badge badge-light"></span>
            </a>
        </div>      
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <div class="container">
            <div class="card">
                <div class="card-body"> 
                        <h3 class="card-title">${detail.getProductName()}</h3>
                        <h6 class="card-subtitle"></h6>
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-6">
                                <div class="white-box text-center"><img src="images/${detail.getLink()}" class="img-responsive"></div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-6">
                                <h4 class="box-title mt-5">Product description</h4>
                                <p>${detail. getProductDes()}</p>
                                <h2 class="mt-5">
                                    $${detail.getProductPrice()}<small class="text-success"></small>
                                </h2>
                                
                                <form action="add1" method="get">
                                     <input class="ok" type="number" name="id" value="${detail.getProductID()}" hidden=""/>
                                <button class="btn btn-primary btn-rounded">Add To Cart</button>
                                </form>
                              
                                <h3 class="box-title mt-5">Key Highlights</h3>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check text-success"></i>Sturdy structure</li>
                                    <li><i class="fa fa-check text-success"></i>Designed to foster easy portability</li>
                                    <li><i class="fa fa-check text-success"></i>Perfect furniture to flaunt your wonderful collectibles</li>
                                </ul>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h3 class="box-title mt-5">General Info</h3>
                                <div class="table-responsive">
                                    <table class="table table-striped table-product">
                                        <tbody>                                       
                                            <tr>
                                                <td>Chip</td>
                                                <td>${detail.getChip()}</td>
                                            </tr>                                      
                                            <tr>
                                                <td>Type</td>
                                                <td>${detail.getProductType()}</td>
                                            </tr>                                     
                                            <tr>
                                                <td>Screen</td>
                                                <td>${detail.getSreen()}</td>
                                            </tr>
                                            <tr>
                                                <td>Camera</td>
                                                <td>${detail.getCamera()}</td>
                                            </tr>                                                                                                                     
                                            <tr>
                                                <td>Mass</td>
                                                <td>${detail.getMass()}</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                  
                </div>
            </div>
        </div>
    </body>
</html>