<%-- 
    Document   : Contact
    Created on : Mar 1, 2023, 8:38:05 PM
    Author     : LEGION
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->
<html>
    <head>
        <title>Contact IPhone</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->

        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,800,900%7cRaleway:300,400,500,600,700" rel="stylesheet">
        <style>
            .pb-100 {
                padding-bottom: 100px;
            }
            .pt-100 {
                padding-top: 100px;
            }
            .mb-100 {
                margin-bottom: 100px;
            }
            a {
                text-decoration: none;
                color: #333;
                -webkit-transition: .4s;
                transition: .4s;
            }
            .section-title {
                position: relative;
            }
            .section-title p {
                font-size: 16px;
                margin-bottom: 5px;
                font-weight: 400;
            }
            .section-title h4 {
                font-size: 40px;
                font-weight: 600;
                text-transform: capitalize;
                position: relative;
                padding-bottom: 20px;
                display: inline-block;
                color: black;
            }
            .section-title h4::before {
                position: absolute;
                content: "";
                width: 80px;
                height: 2px;
                background-color: #d8d8d8;
                bottom: 0;
                left: 50%;
                margin-left: -40px;
            }
            .section-title h4::after {
                position: absolute;
                content: "";
                width: 50px;
                height: 2px;
                background-color: #FF7200;
                left: 0;
                bottom: 0;
                left: 50%;
                margin-left: -25px;
            }
            .contact {
                background-image: url("http://infinityflamesoft.com/html/abal-preview/assets/img/contact_back.jpg");
                background-size: cover;
                background-position: center;
                background-attachment: fixed;
                position: relative;
                z-index: 2;
                color: #fff;
            }
            .contact-form input,
            textarea {
                width: 100%;
                border: 1px solid #555;
                padding: 5px 10px;
                text-transform: capitalize;
                margin-top: 15px;
                background-color: transparent;
                color: #fff;
            }
            .contact:before {
                position: absolute;
                content: "";
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                background-color: #333;
                z-index: -1;
                opacity: .85;
                -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=85)";
            }
            .single-contact {
                text-align: left;
                position: relative;
                padding-left: 70px;
                margin-bottom: 50px;
                margin-top: 10px;
            }
            .single-contact i.fa {
                position: absolute;
                left: 0;
                top: 50%;
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
                background-color: #FF7200;
                width: 40px;
                height: 40px;
                line-height: 40px;
                text-align: center;
                border-radius: 3px;
            }
            .single-contact h5 {
                font-size: 18px;
                margin-bottom: 10px;
                font-weight: 500;
            }
            .single-contact p {
                font-size: 15px;
                font-weight: 400;
            }
            .contact-form input[type="submit"] {
                background-color: #FF7200;
                border: 0px;
                cursor: pointer;
                font-size: 16px;
                -webkit-transition: .4s;
                transition: .4s
            }
            .contact-form input[type="submit"]:hover {
                background-color: #CC5B00
            }
            .contact-form input:focus, textarea:focus {
                border-color: #CC5B00
            }
            .header{
                display: flex
            }
            .link_a ul{
                display: flex;
                list-style-type: none;
                padding-top: 20px;
            }
            .logochanel{
                margin-left: 50px;
                width: calc(25% - 50px);
                height: auto;
                display: flex;
                justify-content: flex-start;
                align-items: center;
            }
            .header img{
                width: 90px;
                height:90px;
            }
            .link_a{
                width: 50%;
            }
            .link_a ul li{
                height: 10vh;
                display: flex;
                justify-content: center;
                align-items: center;
                margin-left: 10px;
            }
            .link_a ul li a{
                text-decoration: none;
                color: green;
                margin-right: 130px;
                font-size:17px;
            }
            .search{
                width: 25%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .single-contact p a{
                color: white;
            }
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #f5f5f5;
            }

            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }

            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }
        </style>
    </head>

    <body>
        <header class="header">              
            <a href="Product.jsp" class="logo">Apple Luxury</a>
            <nav class="nav-items">
                <a href="index.jsp">Home</a>
                <a href="Product.jsp">Product</a>
                <a href="Contact.jsp">Contact</a>
            </nav>
        </header>
        <section class="contact pt-100 pb-100" id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 mx-auto text-center">
                        <div class="section-title mb-100">                          
                            <h4>contact me</h4>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-8">
                        <form action="addcus" class="contact-form">
                            <div class="row">
                                <div class="col-xl-6">
                                    <input type="text" name="name" id="name" placeholder="name">
                                </div>
                                <div class="col-xl-6">
                                    <input type="email" name="email" id="email" placeholder="email">
                                </div>
                                <div class="col-xl-6">
                                    <input type="text" name="address" id="address" placeholder="Address">
                                </div>
                                <div class="col-xl-6">
                                    <input type="text" name="telephone" id="telephone" placeholder="telephone">
                                </div>
                                <div class="col-xl-12">
                                    <textarea placeholder="message" name="mess" cols="30" rows="10"></textarea>
                                    <input type="submit" value="send message" onclick="my_submit()">
                                </div>
                            </div>
                        </form><br>
                        <div class="ggmap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.483649132732!2d105.52468021540224!3d21.013325493679798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2sFPT%20University!5e0!3m2!1sen!2s!4v1677269341448!5m2!1sen!2s" 
                                    width="1200" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-contact">
                            <i class="fa fa-map-marker"></i>
                            <h5>Address</h5>
                            <p><a href="https://hoalac-school.fpt.edu.vn/tin-tuc/tin-fschool/kham-pha-co-so-vat-chat-cua-truong-thpt-fpt-xin-xo-nhat-nhi-ha-noi/#:~:text=Khu%C3%B4n%20vi%C3%AAn%20xanh%20r%E1%BB%99ng%2030%20ha%20Tr%C6%B0%E1%BB%9Dng%20THPT,h%E1%BB%A3p%20th%E1%BB%83%20thao%20v%C3%A0%20c%C3%A1c%20ti%E1%BB%87n%20%C3%ADch%20kh%C3%A1c.">FPT University</a></p>
                        </div>
                        <div class="single-contact">
                            <i class="fa fa-phone"></i>
                            <h5>Phone</h5>
                            <p>(+84) 368 719 647</p>
                        </div>
                        <div class="single-contact">
                            <i class="fa fa-envelope"></i>
                            <h5>Email</h5>
                            <p>bogia1403@gmail.com</p>
                        </div>
                        <div class="single-contact">
                            <i class="fa fa-envelope"></i>
                            <h5>FaceBook</h5>
                            <p><a href="https://www.facebook.com/tuanchuo.14/">Phan Nguyễn Châu Tuấn</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>       
        <script>
            function my_submit() {
                var exampleInputName = document.getElementById("name");
                var exampleInputEmail = document.getElementById("email");
                var exampleInputAddress = document.getElementById("address");
                var exampleInputPhone = document.getElementById("telephone");
                var email = exampleInputEmail.value;
                var name = exampleInputName.value;
                var address = exampleInputAddress.value;
                var phone = exampleInputPhone.value;
                if (email.length == 0 || name.length == 0 || address.length == 0 || phone.length == 0) {
                    if (email.length == 0) {
                        alert("You have not entered Infomatiton");
                        return false;
                    }
                    if (name.length == 0) {
                        alert("You have not entered Name");
                        return false;
                    }
                    if (address.length == 0) {
                        alert("You have not entered Address");
                        return false;
                    }
                    if (phone.length == 0) {
                        alert("You have not entered PhoneNumber");
                        return false;
                    }
                } else {
                    var form = document.getElementById("my_form");
                    form.submit();
                }
            }
        </script>
    </body>
</html>
