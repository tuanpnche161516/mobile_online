

<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "DAO.*" %>
<%@page import = "controller.*" %>
<%@page import = "java.util.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%
ProductDAO x = new ProductDAO(); 
List<Product> lst= x.geProducts();
request.setAttribute("lst", lst);
%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style>
            img{
                width: 200px;
                height: 120px;
            }
            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }

            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }
            .table-title{
                background-color: #52529b;
            }
            .col-sm-6{
                padding-top: 15px;
            }
            .col-sm-6 h2{
                padding-left: 10px;
                color: white;
            }
            .col-sm-6 a{
                margin-left: 50%;
                margin-top: 15px;
            }
        </style>
    <body>
        <header class="header">              
            <a href="Product.jsp" class="logo">Apple Luxury</a>
            <nav class="nav-items">
                <a href="index.jsp">Home</a>
                <a href="Product.jsp">Product</a>
                <a href="Contact.jsp">Contact</a>
            </nav>
        </header>
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Product</b></h2>
                        </div>
                        <div class="col-sm-6">
                            <a href="#addEmployeeModal"  class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Product</span></a>					
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>                           
                            <th>ID</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Quantity</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="x" items="${lst}">
                            <tr>                              
                                <td>${x.getProductID()}</td>
                                <td>${x.getProductName()}</td>
                                <td>
                                    <img src="images/${x.link}">
                                </td>
                                <td>$ ${x.getProductPrice()}</td>
                                <td>$ ${x.getProductDiscount()}</td>
                                <td>${x.getProductQuantity()}</td>
                                <td>
                                    <a href="loadProduct?pid=${x.getProductID()}"  class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                    <a href="delete?pid=${x.getProductID()}" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>

        </div>
        <!-- Edit Modal HTML -->
        <div id="addEmployeeModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="add" method="post">
                        <div class="modal-header">						
                            <h4 class="modal-title">Add Product</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">                          
                            <div class="form-group">
                                <label>Name</label>
                                <input name="name" id="name" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Color</label>
                                <input name="color" id="color" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Cost</label>
                                <input name="cost" id="cost" type="text" class="form-control" onblur="isValidNumber()" required>
                            </div>                          
                            <div class="form-group">
                                <label>Price</label>
                                <input name="price" id="price" type="text" class="form-control" onblur="isValidNumber1()" required>
                            </div>
                            <div class="form-group">
                                <label>Discount</label>
                                <input name="discount" id="discount" type="text" class="form-control" onblur="isValidNumber2()" required>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input name="quantity" id="quantity" type="text" class="form-control" onblur="isValidNumber3()" required>
                            </div>  
                            <div class="form-group">
                                <label>Image</label>
                                <input name="image" id="image" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input name="description" id="description" type="text" class="form-control" required>
                            </div>   
                            <div class="form-group">
                                <label>Chip</label>
                                <input name="chip" id="chip" type="text" class="form-control" required>
                            </div>                                                                                                        
                            <div class="form-group">
                                <label>Screen</label>
                                <input name="screen" id="sreen" type="text" class="form-control" required>
                            </div> 
                            <div class="form-group">
                                <label>Camera</label>
                                <input name="camera" id="camera"  type="text" class="form-control" required>
                            </div> 
                            <div class="form-group">
                                <label>Mass</label>
                                <input name="mass" id="mass" type="text" class="form-control" required>
                            </div> 
                            <div class="form-group">
                                <label>Type</label>
                                <input name="type" id="type" type="text" class="form-control" required>
                            </div>

                        </div> 
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-success" onclick="my_submit()" value="Add">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
           
            function isValidNumber() {
                var exampleInputCost = document.getElementById("cost").value;               
               var regex=/^[/d+]$/;              
                 if (regex.test(exampleInputCost)) {
                   return exampleInputCost;
                } else {                  
                  alert("Cost không hợp lệ");
                }
                 
            }
            function isValidNumber1() {
                var exampleInputPrice = document.getElementById("price").value;             
                var regex=/^[0-9]+$/;
                if (!regex.test(exampleInputPrice)) {
                    alert("Price không hợp lệ");
                     return false;
                } else {
                   alert("");
                    return exampleInputPrice;
                }
            }
            function isValidNumber2() {
                var exampleInputdis = document.getElementById("discount").value;
                var regex=/^[0-9]+$/;
                if (!regex.test(exampleInputdis)) {
                    alert("Discount không hợp lệ");
                    return false;
                } else {
                   alert("");
                    return exampleInputdis;
                }
            }
            function isValidNumber3() {
                var exampleInputquan = document.getElementById("quantity").value;            
                var regex=/^[0-9]+$/;
                if (!regex.test(exampleInputquan)) {
                    alert("Cost không hợp lệ");
                    return false;
                } else {
                   alert("");
                    return exampleInputquan;
                }
            }
            function my_submit() {
                var exampleInputName = document.getElementById("name");
                var exampleInputColor = document.getElementById("color");
                var exampleInputCost = document.getElementById("cost");
                var exampleInputPrice = document.getElementById("price");
                var exampleInputDiscount = document.getElementById("discount");
                var exampleInputQuantity = document.getElementById("quantity");
                var exampleInputImage = document.getElementById("image");
                var exampleInputDescription = document.getElementById("description");
                var exampleInputChip = document.getElementById("chip");
                var exampleInputScreen = document.getElementById("sreen");
                var exampleInputCamera = document.getElementById("camera");
                var exampleInputMass = document.getElementById("mass");
                var exampleInputType = document.getElementById("type");

                var name = exampleInputName.value;
                var color = exampleInputColor.value;
                var cost = exampleInputCost.value;
                var price = exampleInputPrice.value;
                var discount = exampleInputDiscount.value;
                var quantity = exampleInputQuantity.value;
                var Image = exampleInputImage.value;
                var des = exampleInputDescription.value;
                var chip = exampleInputChip.value;
                var screen = exampleInputScreen.value;
                var camera = exampleInputCamera.value;
                var mas = exampleInputMass.value;
                var type = exampleInputPrice.value;

                if (name.length == 0 || color.length == 0 || cost.length == 0 || price.length == 0 || discount.length == 0 || quantity.length == 0
                        || Image.length == 0 || des.length == 0 || chip.length == 0 || screen.length == 0 || camera.length == 0 || mas.length == 0 || type.length == 0) {
                    if (name.length == 0) {
                        alert("Bạn chưa nhập Name");
                        return false;
                    }
                    if (color.length == 0) {
                        alert("Bạn chưa nhập Color");
                        return false;
                    }
                    if (price.length == 0) {
                        alert("Bạn chưa nhập Price");
                        return false;
                    }
                    if (cost.length == 0) {
                        alert("Bạn chưa nhập Cost");
                        return false;
                    }
                    if (price.length == 0) {
                        alert("Bạn chưa nhập Price");
                        return false;
                    }
                    if (discount.length == 0) {
                        alert("Bạn chưa nhập Discount");
                        return false;
                    }
                    if (Image.length == 0) {
                        alert("Bạn chưa nhập Image");
                        return false;
                    }
                    if (des.length == 0) {
                        alert("Bạn chưa nhập Description");
                        return false;
                    }
                    if (chip.length == 0) {
                        alert("Bạn chưa nhập Chip");
                        return false;
                    }
                    if (screen.length == 0) {
                        alert("Bạn chưa nhập Screen");
                        return false;
                    }
                    if (camera.length == 0) {
                        alert("Bạn chưa nhập Camera");
                        return false;
                    }
                    if (mas.length == 0) {
                        alert("Bạn chưa nhập Mass");
                        return false;
                    }
                    if (type.length == 0) {
                        alert("Bạn chưa nhập Type");
                        return false;
                    }
                } else {
                    var form = document.getElementById("my_form");
                    form.submit();
                }
            }
        </script>
    </body>
</html>