<%-- 
    Document   : Login
    Created on : Mar 3, 2023, 1:21:21 AM
    Author     : LEGION
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>

    </head>
    <body>
        <section class="vh-100" style="background-color: #9A616D;">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col col-xl-10">
                        <div class="card" style="border-radius: 1rem;">
                            <div class="row g-0">
                                <div class="col-md-6 col-lg-5 d-none d-md-block">
                                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/img1.webp"
                                         alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
                                </div>
                                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                                    <div class="card-body p-4 p-lg-5 text-black">

                                        <form action="login" method="post">

                                            <div class="d-flex align-items-center mb-3 pb-1">
                                                <i class="fas fa-cubes fa-2x me-3" style="color: #ff6219;"></i>
                                                <span class="h1 fw-bold mb-0">Apple Luxury</span>
                                            </div>

                                            <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;"></h5>

                                            <div class="form-outline mb-4">
                                                <input type="text" id="email" name="email" class="form-control form-control-lg" />
                                                <label class="form-label" for="form2Example17">Email</label>
                                            </div>

                                            <div class="form-outline mb-4">
                                                <input type="password" id="pass" name="pass" class="form-control form-control-lg" />
                                                <label class="form-label" for="form2Example27">Password</label>
                                            </div>

                                            <div class="pt-1 mb-4">
                                                <input type="submit" value="Login" onclick="isValidSignIn()"></br>
                                            </div>                 
                                            <p class="mb-5 pb-lg-2" style="color: #393f81;">Don't have an account? <a href="Register.jsp"
                                                                                                                      style="color: #393f81;">Register here</a></p>                                           
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            function my_submit() {
                var exampleInputEmail = document.getElementById("email");
                var exampleInputPass = document.getElementById("pass");
                var email = exampleInputEmail.value;
                var pass = exampleInputPass.value;
                if (email.length == 0 || pass.length == 0) {
                    if (email.length == 0) {
                        alert("Bạn chưa nhập Email");
                        return false;
                    }
                    if (pass.length == 0) {
                        alert("Bạn chưa nhập PassWord");
                        return false;
                    }
                } else {
                    var form = document.getElementById("my_form");
                    form.submit();
                }
            }
            function isValidSignIn(username, password, recaptchaResponse) {
                $.ajax({
                    url: "checkauth",
                    type: "POST",
                    data: {
                        username: username, password: password, captcha: recaptchaResponse
                    },
                    success: function (response) {
                        if (username.length === 0) {
                            modalMessage.innerHTML = "please enter username!";

                        } else if (password.length === 0) {
                            modalMessage.innerHTML = "please enter password!";

                        } else {
                            if (response === "false") {
                                modalMessage.innerHTML = "Captcha required";
                            } else {
                                if (response === "not confirmed") {
                                    modalMessage.innerHTML = "This account has not been activated, select forgot password to activate this account!";
                                    grecaptcha.reset();
                                } else if (response === "invalid") {
                                    modalMessage.innerHTML = "This account is invalid!";
                                    grecaptcha.reset();
                                } else {
                                    grecaptcha.reset();
                                    hideModal();

                                    signinForm.submit();
                                }


                            }

                        }
                        showModal();
                    },
                    error: function () {
                        console.log("Error checking username availability");
                    }
                });
            }
        </script>

    </body>
</html>


