<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Simple HTML HomePage</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <style>
            body {
                margin: 0;
                box-sizing: border-box;
            }

            /* CSS for header */
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #f5f5f5;
            }

            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }

            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }

            /* CSS for main element */
            .intro {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                width: 100%;
                height: 520px;
                background: linear-gradient(to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.5) 100%), url("https://images.unsplash.com/photo-1587620962725-abab7fe55159?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1031&q=80");
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
            }

            .intro h1 {
                font-family: sans-serif;
                font-size: 60px;
                color: #fff;
                font-weight: bold;
                text-transform: uppercase;
                margin: 0;
            }

            .intro p {
                font-size: 20px;
                color: #d1d1d1;
                text-transform: uppercase;
                margin: 20px 0;
            }

            .intro button {
                background-color: #5edaf0;
                color: #000;
                padding: 10px 25px;
                border: none;
                border-radius: 5px;
                font-size: 20px;
                font-weight: bold;
                cursor: pointer;
                box-shadow: 0px 0px 20px rgba(255, 255, 255, 0.4)
            }

            .achievements {
                display: flex;
                justify-content: space-around;
                align-items: center;
                padding: 40px 80px;
                background-color: #f5f5f5;
            }

            .achievements .work {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                padding: 0 40px;
            }

            .achievements .work i {
                width: fit-content;
                font-size: 50px;
                color: #333333;
                border-radius: 50%;
                border: 2px solid #333333;
                padding: 12px;
            }

            .achievements .work .work-heading {
                font-size: 20px;
                color: #333333;
                text-transform: uppercase;
                margin: 10px 0;
            }

            .achievements .work .work-text {
                font-size: 15px;
                color: #585858;
                margin: 10px 0;
            }

            .about-me {
                display: flex;
                justify-content: center;
                align-items: center;
                padding: 40px 80px;
                border-top: 2px solid #eeeeee;
                background-color: #f5f5f5;
            }

            .about-me img {
                width: 300px;
                max-width: 90%;
                height: auto;
                border-radius: 100%;
            }

            .about-me-text h2 {
                font-size: 30px;
                color: #333333;
                text-transform: uppercase;
                margin: 0;
            }

            .about-me-text p {
                font-size: 15px;
                color: #585858;
                margin: 10px 0;
            }

            /* CSS for footer */
            .footer {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #302f49;
                padding: 40px 80px;
            }

            .footer .copy {
                color: #fff;
            }

            .bottom-links {
                display: flex;
                justify-content: space-around;
                align-items: center;
                padding: 40px 0;
            }

            .bottom-links .links {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                padding: 0 40px;
            }

            .bottom-links .links span {
                font-size: 20px;
                color: #fff;
                text-transform: uppercase;
                margin: 10px 0;
            }

            .bottom-links .links a {
                text-decoration: none;
                color: #a1a1a1;
                padding: 10px 20px;
            }
            * {
                box-sizing: border-box
            }
            body {
                font-family: Verdana, sans-serif;
                margin:0
            }
            .mySlides {
                display: none
            }
            img {
                vertical-align: middle;
            }

            /* Slideshow cont0ainer */
            .slideshow-container {
                width: 100%;
                position: relative;
                margin: auto;
                height: 10%;
            }

            /* Next & previous buttons */
            .prev, .next {
                cursor: pointer;
                position: absolute;
                top: 50%;
                width: auto;
                margin-top: -22px;
                padding: 16px;
                color: white;
                font-weight: bold;
                font-size: 18px;
                transition: 0.6s ease;
                border-radius: 0 3px 3px 0;
                user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
                right: 0;
                border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover, .next:hover {
                background-color: rgba(0,0,0,0.8);
            }

            /* Caption text */
            .text {
                color: #f2f2f2;
                font-size: 15px;
                padding: 8px 12px;
                position: absolute;
                bottom: 8px;
                width: 100%;
                text-align: center;
            }

            /* Number text (1/3 etc) */
            .numbertext {
                color: #f2f2f2;
                font-size: 12px;
                padding: 8px 12px;
                position: absolute;
                top: 0;
            }

            /* The dots/bullets/indicators */
            .dot {
                cursor: pointer;
                height: 15px;
                width: 15px;
                margin: 0 2px;
                background-color: #bbb;
                border-radius: 50%;
                display: inline-block;
                transition: background-color 0.6s ease;
            }

            .active, .dot:hover {
                background-color: #717171;
            }

            /* Fading animation */
            .fade {
                animation-name: fade;
                animation-duration: 1.5s;
            }

            @keyframes fade {
                from {
                    opacity: .4
                }
                to {
                    opacity: 1
                }
            }
        </style>
    </head>      
    <html lang="en">
        <body>
            <header class="header">               
                <a href="" class="logo">Apple Luxury</a>
                <nav class="nav-items">
                    <a href="index.jsp">Home</a>
                    <a href="Product.jsp">Product</a>
                    <a href="Contact.jsp">Contact</a>
                    <c:if test="${sessionScope.acc == null}">
                    <a href="Login.jsp">Login</a>
                     </c:if>
                    <c:if test="${sessionScope.acc != null}">
                     <a href="logout">Logout</a>
                    </c:if>
                </nav>
            </header>
            <main>
                <div class="slideshow-container.">

                    <div class="mySlides fade">
                        <div class="numbertext"> 1 / 3</div>
                        <img src="images/img4.png" style="width:100%">
                        <div class="text">Caption Text</div>
                    </div>


                    <div class="mySlides fade">
                        <div class="numbertext">2 / 3</div>
                        <img src="images/img5.png" style="width:100%">
                        <div class="text">Caption Two</div>
                    </div>

                    <div class="mySlides fade">
                        <div class="numbertext">3 / 3</div>
                        <img src="images/img6.png" style="width:100%">
                        <div class="text">Caption Three</div>
                    </div>                     
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>

                </div>
                
                <div style="text-align:center">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                    <span class="dot" onclick="currentSlide(3)"></span>
                </div>
                <div class="achievements">
                    <div class="work">
                        <i class="fas fa-atom"></i>
                        <p class="work-heading">Projects</p>
                        <p class="work-text">I am an iphone business for many years and i always maintain my opinion on my business and always innovate and update the fastest information about my products.</p>
                    </div>
                    <div class="work">
                        <i class="fas fa-skiing"></i>
                        <p class="work-heading">Service</p>
                        <p class="work-text">We always have and will bring our customers the best experience.</p>
                    </div>
                    <div class="work">
                        <i class="fas fa-ethernet"></i>
                        <p class="work-heading">Product</p>
                        <p class="work-text">I always carefully check my products and bring peace of mind to customers.</p>
                    </div>
                </div>
                <div class="about-me">
                    <div class="about-me-text">
                        <h2>About Me</h2>
                        <p>I love technology and I want to make my company more and more developed, always creative and innovative to be able to go further in the future.</p>
                    </div>
                    <img src="images/e17c7f5fce190d475408.png" alt="me">
                </div>
            </main>
            <footer class="footer">
                <div class="copy">© 2023 Apple Luxury</div>
                <div class="bottom-links">
                    <div class="links">
                        <span>More Info</span>
                        <a href="index.html">Home</a>
                        <a href="Product.jsp">Product</a>
                        <a href="Contact.jsp">Contact</a>
                    </div>
                    <div class="links">
                        <span>Social Links</span>
                        <a href="https://www.facebook.com/tuanchuo.14/"><i class="fab fa-facebook"></i></a>
                        <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.instagram.com/tuanchuo.03.14.02/"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </footer>
            <script>
                let slideIndex = 0;
                showSlides();

                function showSlides() {
                    let i;
                    let slides = document.getElementsByClassName("mySlides");
                    let dots = document.getElementsByClassName("dot");
                    for (i = 0; i < slides.length; i++) {
                        slides[i].style.display = "none";
                    }
                    slideIndex++;
                    if (slideIndex > slides.length) {
                        slideIndex = 1
                    }
                    for (i = 0; i < dots.length; i++) {
                        dots[i].className = dots[i].className.replace(" active", "");
                    }
                    slides[slideIndex - 1].style.display = "block";
                    dots[slideIndex - 1].className += " active";
                    setTimeout(showSlides, 2000); // Change image every 2 seconds
                }
            </script>
        </body>

    </html>


