<%-- 
    Document   : Product2
    Created on : Mar 4, 2023, 2:34:02 AM
    Author     : LEGION
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "DAO.*" %>
<%@page import = "controller.*" %>
<%@page import = "java.util.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<!DOCTYPE html>
<%
     ProductDAO x = new ProductDAO(); 
  List<Product> lst = x.geProducts();
  request.setAttribute("lst", lst);
%> 
<html>
    <head>
        <meta charset="utf-8">
        <title>IphoneShop.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/js/bootstrap.bundle.min.js"></script> 
        <style type="text/css">
            .no-cart a img{
                width: 3%;
                margin-left: 90%;
                margin-top: 10px;
                color: orange;
            }
            .no-cart a{
                margin-top: 200px;
                color: orange;
            }
            .header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #f5f5f5;
            }

            .header .logo {
                font-size: 25px;
                font-family: "Sriracha", cursive;
                color: #000;
                text-decoration: none;
                margin-left: 30px;
            }
            .nav-items {
                display: flex;
                justify-content: space-around;
                align-items: center;
                background-color: #f5f5f5;
                margin-right: 20px;
            }

            .nav-items a {
                text-decoration: none;
                color: #000;
                padding: 35px 20px;
            }
            .sale{
                font-size: 18px;
                background:orange;
                margin-left: 82%;
                border-radius: 10%;
            }
            .logochanel{
                margin-left: 50px;
                width: calc(20% - 40px);
                height: 30px;
                display: flex;
                justify-content: flex-start;
                align-items: center;
                padding-top: 40px;
            }
            .header img{
                width: 90px;
                height:90px;
            }
            .link_a{
                width: 50%;
            }
            .link_a ul li{
                height: 10vh;
                display: flex;
                justify-content: center;
                align-items: center;
                margin-left: 10px;
            }
            .link_a ul li a{
                text-decoration: none;
                color: green;
                margin-right: 130px;
                font-size: large;
            }
            .search{
                width: 25%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .search button{
                background-color: black;
                color: white;
                padding: 1 5px;
            }
            .footer{
                text-align: center;
                color: white;
                background: black;
                padding-bottom: 10px;
                padding-top: 10px;
                margin-top: 10px;
                margin-bottom: 10px;
            }
            body{
                padding-top:20px;

            }
            .c_review {
                margin-bottom: 0
            }

            .c_review li {
                margin-bottom: 16px;
                padding-bottom: 13px;
                border-bottom: 1px solid #e8e8e8
            }

            .c_review li:last-child {
                margin: 0;
                border: none
            }

            .c_review .avatar {
                float: left;
                width: 80px
            }

            .c_review .comment-action {
                float: left;
                width: calc(100% - 80px)
            }

            .c_review .comment-action .c_name {
                margin: 0
            }

            .c_review .comment-action p {
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                max-width: 95%;
                display: block
            }

            .product_item:hover .cp_img {
                top: -40px
            }

            .product_item:hover .cp_img img {
                box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22)
            }

            .product_item:hover .cp_img .hover {
                display: block
            }

            .product_item .cp_img {
                position: absolute;
                top: 0px;
                left: 50%;
                transform: translate(-50%);
                -webkit-transform: translate(-50%);
                -ms-transform: translate(-50%);
                -moz-transform: translate(-50%);
                -o-transform: translate(-50%);
                -khtml-transform: translate(-50%);
                width: 100%;
                padding: 15px;
                transition: all 0.2s ease-in-out;
            }

            .product_item .cp_img img {
                transition: all 0.2s ease-in-out;
                border-radius: 6px;
                width: 70%;
                margin-left: 20%;
            }

            .product_item .cp_img .hover {
                display: none;
                text-align: center;
                margin-top: 10px
            }

            .product_item .product_details {
                padding-top: 110%;
                text-align: center
            }

            .product_item .product_details h5 {
                margin-bottom: 5px
            }

            .product_item .product_details h5 a {
                font-size: 16px;
                color: #444
            }

            .product_item .product_details h5 a:hover {
                text-decoration: none
            }

            .product_item .product_details .product_price {
                margin: 0
            }

            .product_item .product_details .product_price li {
                display: inline-block;
                padding: 0 10px
            }

            .product_item .product_details .product_price .new_price {
                font-weight: 600;
                color: #ff4136
            }

            .product_item_list table tr td {
                vertical-align: middle
            }

            .product_item_list table tr td h5 {
                font-size: 15px;
                margin: 0
            }

            .product_item_list table tr td .btn {
                box-shadow: none !important
            }

            .product-order-list table tr th:last-child {
                width: 145px
            }

            .preview {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            .preview .preview-pic {
                -webkit-box-flex: 1;
                -webkit-flex-grow: 1;
                -ms-flex-positive: 1;
                flex-grow: 1
            }

            .preview .preview-thumbnail.nav-tabs {
                margin-top: 15px;
                font-size: 0
            }

            .preview .preview-thumbnail.nav-tabs li {
                width: 20%;
                display: inline-block
            }

            .preview .preview-thumbnail.nav-tabs li nav-link img {
                max-width: 100%;
                display: block
            }

            .preview .preview-thumbnail.nav-tabs li a {
                padding: 0;
                margin: 2px;
                border-radius: 0 !important;
                border-top: none !important;
                border-left: none !important;
                border-right: none !important
            }

            .preview .preview-thumbnail.nav-tabs li:last-of-type {
                margin-right: 0
            }

            .preview .tab-content {
                overflow: hidden
            }

            .preview .tab-content img {
                width: 100%;
                -webkit-animation-name: opacity;
                animation-name: opacity;
                -webkit-animation-duration: .3s;
                animation-duration: .3s
            }

            .details {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            .details .rating .stars {
                display: inline-block
            }

            .details .sizes .size {
                margin-right: 10px
            }

            .details .sizes .size:first-of-type {
                margin-left: 40px
            }

            .details .colors .color {
                display: inline-block;
                vertical-align: middle;
                margin-right: 10px;
                height: 2em;
                width: 2em;
                border-radius: 2px
            }

            .details .colors .color:first-of-type {
                margin-left: 20px
            }

            .details .colors .not-available {
                text-align: center;
                line-height: 2em
            }

            .details .colors .not-available:before {
                font-family: Material-Design-Iconic-Font;
                content: "\f136";
                color: #fff
            }

            @media screen and (max-width: 996px) {
                .preview {
                    margin-bottom: 20px
                }
            }

            @-webkit-keyframes opacity {
                0% {
                    opacity: 0;
                    -webkit-transform: scale(3);
                    transform: scale(3)
                }
                100% {
                    opacity: 1;
                    -webkit-transform: scale(1);
                    transform: scale(1)
                }
            }

            @keyframes opacity {
                0% {
                    opacity: 0;
                    -webkit-transform: scale(3);
                    transform: scale(3)
                }
                100% {
                    opacity: 1;
                    -webkit-transform: scale(1);
                    transform: scale(1)
                }
            }

            .cart-page .cart-table tr th:last-child {
                width: 145px
            }

            .cart-table .quantity-grp {
                width: 120px
            }

            .cart-table .quantity-grp .input-group {
                margin-bottom: 0
            }

            .cart-table .quantity-grp .input-group-addon {
                padding: 0 !important;
                text-align: center;
                background-color: #1ab1e3
            }

            .cart-table .quantity-grp .input-group-addon a {
                display: block;
                padding: 8px 10px 10px;
                color: #fff
            }

            .cart-table .quantity-grp .input-group-addon a i {
                vertical-align: middle
            }

            .cart-table .quantity-grp .form-control {
                background-color: #fff
            }

            .cart-table .quantity-grp .form-control+.input-group-addon {
                background-color: #1ab1e3
            }

            .ec-checkout .wizard .content .form-group .btn-group.bootstrap-select.form-control {
                padding: 0
            }

            .ec-checkout .wizard .content .form-group .btn-group.bootstrap-select.form-control .btn-round.btn-simple {
                padding-top: 12px;
                padding-bottom: 12px
            }

            .ec-checkout .wizard .content ul.card-type {
                font-size: 0
            }

            .ec-checkout .wizard .content ul.card-type li {
                display: inline-block;
                margin-right: 10px
            }

            .card {
                background: #fff;
                margin-bottom: 30px;
                transition: .5s;
                border: 0;
                border-radius: .55rem;
                position: relative;
                width: 100%;
                box-shadow: 0 1px 2px 0 rgba(0,0,0,0.1);
            }

            .card .body {
                font-size: 14px;
                color: #424242;
                padding: 20px;
                font-weight: 400;
            }
            .header__cart-icon {
                padding: 5px;
            }

            .header__cart-icon:hover {
                color: #e4a294;

            }

            .header__cart-icon:hover .header__cart-notice {
                background-color: #e4a294;

            }

            /* .header__cart-notice:hover {
                background-color: #e4a294 ;
            } */

            /* .fas.fa-shopping-cart:hover {
                color: #e4a294;
                cursor: pointer;
            } */
            /* .navbar li > i:hover */
            /* .header__cart:hover {
                color: #e4a294;
                cursor: pointer;
            } */
            .navbar > li.active:nth-child(1):after,
            .navbar > li.active:nth-child(2):after,
            .navbar > li.active:nth-child(3):after {
                content: "";
                width: 60%;
                height: 2px;
                background-color: #EE4D2D;
                position: absolute;
                bottom: -4px;
                left: 18px;
                scale: 0.9;
            }

            .navbar > li.active:last-child {
                /* color: #EE4D2D; */
                color: rgb(214, 66, 66);
            }

            .navbar > li.active:last-child .header__cart-notice {
                background-color: #d6492d;
                /* color: #EE4D2D;
                border: 2px solid ; */
            }

            .fas.fa-shopping-cart {
                font-size: 18px;
                /* padding: 5px; */
            }
          
        </style>
    </head>       
    <body>  
        <div class="header">
            <a href="Product.jsp" class="logo">Apple Luxury</a>
            <nav class="nav-items">
                <a href="index.jsp">Home</a>
                <a href="Product.jsp">Product</a>
                <a href="Contact.jsp">Contact</a>
                <c:if test="${sessionScope.acc.isAdmin == 1}">
                <a href="ManagerProduct.jsp">Manager Product</a>
                </c:if>
            </nav>
           <form action="search" method="post" class="form-inline my-2 my-lg-0">
                <div class="input-group input-group-sm">
                    <input value="${txtS}" name="Search" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Search...">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary btn-number">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <a class="btn btn-success btn-sm ml-3" href="gui">
                    <i class="fa fa-shopping-cart"></i> Cart
                    <span class="badge badge-light"></span>
                </a>
            </form>          
        </div>          
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">     

        <div class="container">
            <div class="row clearfix">
                <c:forEach var="x" items="${lst}">
                    <div class="col-lg-3 col-md-4 col-sm-12">
                        <div class="card product_item">
                            <form action="add1" method="get">
                                <input class="ok" type="number" name="id" value="${x.getProductID()}" hidden=""/>
                            <div class="body">                              
                                <div class="cp_img">                                   
                                    <img src="images/${x.link}" alt="Product" class="img-fluid">            
                                    <div class="hover">
                                        <button type="" class="btn btn-primary btn-sm waves-effect"><i class="zmdi zmdi-plus" ></i></a</button>
                                    </div>
                                </div>

                                <div class="product_details">
                                    <h5><a href="detail?pid=${x.getProductID()}"><u>${x.getProductName()}</u></a></h5>
                                    <ul class="product_price list-unstyled">
                                        <li class="old_price">Quantity: ${x.getProductQuantity()}</li>
                                        <li class="new_price"> $ ${x.getProductPrice()}</li>                                   
                                    </ul>
                                </div>                              
                            </div>
                                    </form>
                        </div>
                    </div>
                </c:forEach>
            </div>                     
        </div>                      

    </body>
</html>
