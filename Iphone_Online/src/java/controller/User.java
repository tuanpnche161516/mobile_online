package controller;

public class User {

    private int IDCustomer;
    private String customerName;
    private String Pass;
    private int isAdmin;

    public User() {
    }

    public User(int IDCustomer, String customerName, String Pass, int isAdmin) {
        this.IDCustomer = IDCustomer;
        this.customerName = customerName;
        this.Pass = Pass;
        this.isAdmin = isAdmin;
    }

    public int getIDCustomer() {
        return IDCustomer;
    }

    public void setIDCustomer(int IDCustomer) {
        this.IDCustomer = IDCustomer;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }
    

}
