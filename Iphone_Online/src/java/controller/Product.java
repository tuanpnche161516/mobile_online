/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

/**
 *
 * @author LEGION
 */
public class Product {

    private int productID;
    private String productName;
    private String productColor;
    private int productCost;
    private int productPrice;
    private int productDiscount;
    private int productQuantity;
    private String link;
    private String productDes;
    private String Chip;  
    private String Sreen;
    private String Camera;   
    private String Mass;
    private String productType;

    public Product() {
    }

    public Product(int productID, String productName, String productColor, int productCost, int productPrice, int productDiscount, int productQuantity, String link, String productDes, String Chip, String Sreen, String Camera, String Mass, String productType) {
        this.productID = productID;
        this.productName = productName;
        this.productColor = productColor;
        this.productCost = productCost;
        this.productPrice = productPrice;
        this.productDiscount = productDiscount;
        this.productQuantity = productQuantity;
        this.link = link;
        this.productDes = productDes;
        this.Chip = Chip;
        this.Sreen = Sreen;
        this.Camera = Camera;
        this.Mass = Mass;
        this.productType = productType;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductColor() {
        return productColor;
    }

    public void setProductColor(String productColor) {
        this.productColor = productColor;
    }

    public int getProductCost() {
        return productCost;
    }

    public void setProductCost(int productCost) {
        this.productCost = productCost;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(int productDiscount) {
        this.productDiscount = productDiscount;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getProductDes() {
        return productDes;
    }

    public void setProductDes(String productDes) {
        this.productDes = productDes;
    }

    public String getChip() {
        return Chip;
    }

    public void setChip(String Chip) {
        this.Chip = Chip;
    }

    public String getSreen() {
        return Sreen;
    }

    public void setSreen(String Sreen) {
        this.Sreen = Sreen;
    }

    public String getCamera() {
        return Camera;
    }

    public void setCamera(String Camera) {
        this.Camera = Camera;
    }

    public String getMass() {
        return Mass;
    }

    public void setMass(String Mass) {
        this.Mass = Mass;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }    
}
