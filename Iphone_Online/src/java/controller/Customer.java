/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

/**
 *
 * @author LEGION
 */
public class Customer {

    private int IDCustomer;
    private String Name;
    private String Phone;
    private String Email;
    private String Pass;
    private String Address;
    private String message;
    public Customer() {
    }

    public Customer(int IDCustomer, String Name, String Phone, String Email, String Pass, String Address, String message) {
        this.IDCustomer = IDCustomer;
        this.Name = Name;
        this.Phone = Phone;
        this.Email = Email;
        this.Pass = Pass;
        this.Address = Address;
        this.message = message;
    }

    public int getIDCustomer() {
        return IDCustomer;
    }

    public void setIDCustomer(int IDCustomer) {
        this.IDCustomer = IDCustomer;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

   

}
