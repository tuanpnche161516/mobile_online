/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import com.sun.corba.se.spi.presentation.rmi.PresentationManager;
import controller.Cart;
import controller.Customer;
import controller.Item;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import model.MyDAO;

/**
 *
 * @author LEGION
 */
public class OrderDAO extends MyDAO{
   public void addOrder(Customer c,Cart cart){
       LocalDate curDate=LocalDate.now();
       String date=curDate.toString();
       try{
           xSql="insert into [Orders] values(?,?,?)";
           ps = con.prepareStatement(xSql);
            ps.setString(1, date);
            rs = ps.executeQuery();
            ps.setInt(2, c.getIDCustomer());
            ps.setInt(3, cart.getTotalMoney());
           String sql1="select top 1 orderID from [Orders] order by orderID desc" ;
      PreparedStatement st1=connection.prepareStatement(sql1);
           ResultSet rs=st1.executeQuery();
           // add bang oderDetails
           if(rs.next()){
               int oid=rs.getInt("orderID");
               for (Item i : cart.getItems()) {
                   String sql2="insert into [OrderDetails] values(?,?,?,?)";
                 PreparedStatement st2= connection.prepareStatement(sql2); 
                 st2.setInt(1, oid);
                 st2.setInt(2, i.getProduct().getProductID());
                st2.setInt(3, i.getQuantity());
                 st2.setInt(4, i.getPrice());
                 st2.executeUpdate();
               }
           }
           // cap nhat lai so luong sp
           String sql3="update Product set productQuantity=productQuantity-? where productID=?";
           PreparedStatement st3=connection.prepareStatement(sql3);
           for (Item i : cart.getItems()) {
               st3.setInt(1, i.getQuantity());
               st3.setInt(2, i.getProduct().getProductID());
               st3.executeUpdate();
           }
       }catch(SQLException e){
           
       }
   }
}
