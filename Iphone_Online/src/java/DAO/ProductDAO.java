/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import controller.Cart;
import controller.Customer;
import controller.Item;
import controller.Product;
import controller.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import model.MyDAO;
import java.util.ArrayList;
import java.util.List;
import model.MyDAO;

/**
 *
 * @author LEGION
 */
public class ProductDAO extends MyDAO {

    public List<Product> geProducts() {
        List<Product> t = new ArrayList<>();
        xSql = "select * from Product";

        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();

            Product x;
            while (rs.next()) {

                x = new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14)
                );
                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (t);
    }

    public Product geProductsID(int id) {
        xSql = "select * from Product where productID =? ";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                return new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14)
                );
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteProduct(int pid) {
        xSql = "delete from Product where productID = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, pid);
            rs = ps.executeQuery();
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public int getLastProductId(){
         xSql = "select top 1 productID from Product order by productID desc";
         int n=0;
         try {
            ps = con.prepareStatement(xSql);          
            rs = ps.executeQuery();
            while(rs.next()){
                n=rs.getInt("productID");
            }
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
         return n;
    }
    public void InsertProduct(Product x) {
        int id=getLastProductId()+1;
        xSql = "insert into Product ("+"productID"+",productName,productColor"
                + ",productCost,productPrice,productDiscount,"
                + " productQuantity,link,productDes,Chip,Screen"
                + ",Camera, Mass,productType)"
                + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ps.setString(2, x.getProductName());
            ps.setString(3, x.getProductColor());
            ps.setInt(4, x.getProductCost());
            ps.setInt(5, x.getProductPrice());
            ps.setInt(6, x.getProductDiscount());
            ps.setInt(7, x.getProductQuantity());
            ps.setString(8, x.getLink());
            ps.setString(9, x.getProductDes());
            ps.setString(10, x.getChip());
            ps.setString(11, x.getSreen());
            ps.setString(12, x.getCamera());
            ps.setString(13, x.getMass());
            ps.setString(14, x.getProductType());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void UpdateProduct(String productName, String productColor, int productCost, int productPrice, int productDiscount,
            int productQuantity, String link,int productID) {
        xSql = "update Product set [productName]='" + productName + "',[productColor]='" + productColor + "?',\n"
                + "productCost=" + productCost + ",productPrice=" + productPrice + ",productDiscount=" + productDiscount + ",productQuantity=" + productQuantity + ",[link]='" + link + "'\n"
                + "where productID=" + productID;
        try {
            ps = con.prepareStatement(xSql);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<Product> searchByName(String txt) {
        List<Product> t = new ArrayList<>();
        xSql = "select * from Product where productName like '%" + txt.trim() + "%'";

        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            Product x;
            while (rs.next()) {
                x = new Product(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14)
                );
                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (t);
    }
     public void addOrder(Customer c,Cart cart){
       LocalDate curDate=LocalDate.now();
       String date=curDate.toString();
       try{
           xSql="insert into [Orders] values(?,?,?)";
           ps = con.prepareStatement(xSql);
            ps.setString(1, date);
            rs = ps.executeQuery();
            ps.setInt(2, c.getIDCustomer());
            ps.setInt(3, cart.getTotalMoney());
           String sql1="select top 1 orderID from [Orders] order by orderID desc" ;
      PreparedStatement st1=connection.prepareStatement(sql1);
           ResultSet rs=st1.executeQuery();
           // add bang oderDetails
           if(rs.next()){
               int oid=rs.getInt("orderID");
               for (Item i : cart.getItems()) {
                   String sql2="insert into [OrderDetails] values(?,?,?,?)";
                 PreparedStatement st2= connection.prepareStatement(sql2); 
                 st2.setInt(1, oid);
                 st2.setInt(2, i.getProduct().getProductID());
                st2.setInt(3, i.getQuantity());
                 st2.setInt(4, i.getPrice());
                 st2.executeUpdate();
               }
           }
           // cap nhat lai so luong sp
           String sql3="update Product set productQuantity=productQuantity-? where productID=?";
           PreparedStatement st3=connection.prepareStatement(sql3);
           for (Item i : cart.getItems()) {
               st3.setInt(1, i.getQuantity());
               st3.setInt(2, i.getProduct().getProductID());
               st3.executeUpdate();
           }
       }catch(SQLException e){
           
       }
   }
              
}
