/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import model.MyDAO;

/**
 *
 * @author LEGION
 */
public class OrderDeitalsDAO extends MyDAO{
     public int getLastOrderId(){
         xSql = "select top 1 orderID from OrderDetails order by orderID desc";
         int n=0;
         try {
            ps = con.prepareStatement(xSql);          
            rs = ps.executeQuery();
            while(rs.next()){
                n=rs.getInt("orderID");
            }
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
         return n;
    }
}
