/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import model.MyDAO;
import controller.Customer;
import controller.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LEGION
 */
public class CustomerDao extends MyDAO {
     
    public int getLastCustomerId() {
        xSql = "select top 1 customerID from Customer order by customerID desc";
        int n = 0;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                n = rs.getInt("customerID");
            }
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return n;
    }

    public void InsertCustomer(Customer x) {
        int id = getLastCustomerId() + 1;
        xSql = "INSERT INTO Customer ("+"customerID"+", customerName, customerPhone, customerEmail,customerPass,customerAddress,message)\n"
                + "VALUES (?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ps.setString(2, x.getName());
            ps.setString(3, x.getPhone());
            ps.setString(4, x.getEmail());
            ps.setString(5, x.getPass());
             ps.setString(6, x.getAddress());
             ps.setString(7, x.getMessage());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
