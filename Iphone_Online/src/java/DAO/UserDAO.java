package DAO;

import controller.Product;
import controller.User;
import java.util.*;
import model.MyDAO;
import model.MyDAO;

public class UserDAO extends MyDAO {
    
    public List<User> getUsers() {
        List<User> t = new ArrayList<User>();
        xSql = "select * from Account";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            String xName, xPass;
            int xRole;
            User x;
            while (rs.next()) {
                xRole = rs.getInt("IDCustomer");                
                xName = rs.getString("customerName");                
                xPass = rs.getString("Pass");
                x = new User(xRole, xName, xPass, xRole);
                t.add(x);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (t);
    }
    
    public User getUser(String xName, String xPass) {
        xSql = "select * from  Account where customerName = ? and Pass = ?";
        
        int xRole;
        User x = null;
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, xName);
            ps.setString(2, xPass);
            rs = ps.executeQuery();
            /* The cursor on the rs after this statement is in the BOF area, i.e. it is before the first record.
         Thus the first rs.next() statement moves the cursor to the first record
             */
            
            if (rs.next()) {                
                xRole = rs.getInt("IDCustomer");
                x = new User(xRole, rs.getString(2), rs.getString(3), rs.getInt(4));
            }
            
            rs.close();
            ps.close();
        } catch (Exception e) {
        }
        return (x);        
    }    

    public int getLastAccId() {
        xSql = "select top 1 IDCustomer from Account order by IDCustomer desc";
        int n = 0;
        try {
            ps = con.prepareStatement(xSql);            
            rs = ps.executeQuery();
            while (rs.next()) {
                n = rs.getInt("IDCustomer");
            }
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return n;
    }

    public void InsertAcc(User x) {
        int id = getLastAccId() + 1;
        xSql = "insert into Account (" + "IDCustomer" + ",customerName,Pass)"
                + " values (?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ps.setString(2, x.getCustomerName());
            ps.setString(3, x.getPass());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
