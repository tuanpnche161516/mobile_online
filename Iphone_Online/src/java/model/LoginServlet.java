package model;

import DAO.UserDAO;
import controller.User;
import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

import jakarta.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xName = request.getParameter("email");
        String xPass = request.getParameter("pass");
        
        
        User x;
        UserDAO t = new UserDAO();
        x = t.getUser(xName, xPass);    
        
                
            HttpSession session=request.getSession();
            session.setAttribute("acc", x);
            session.setMaxInactiveInterval(30 * 60);
            request.getRequestDispatcher("/index.jsp").include(request, response);
        
        
       
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("Login.jsp").forward(request, response);

    }
}
