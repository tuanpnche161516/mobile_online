/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package model;

import DAO.ProductDAO;
import controller.Cart;
import controller.Item;
import controller.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author LEGION
 */
@WebServlet(name = "DelSum", urlPatterns = {"/del"})
public class DelSum extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         processRequest(request, response);     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       int id = Integer.valueOf(request.getParameter("id"));
        String size = request.getParameter("size");
        ProductDAO pd = new ProductDAO();
        PrintWriter out = response.getWriter();
        List<Product> lst = pd.geProducts();
        Cookie arr[] = request.getCookies();
        String txt = "";
        for (Cookie o : arr) {
            if (o.getName().equals("cart")) {
                txt = txt + o.getValue();
                o.setMaxAge(0);
                response.addCookie(o);
            }
        }
        String s1 = "";
        String[] s = txt.split("/");
        for (String i : s) {
            String[] n = i.split(":", 0);
            int id1 = Integer.parseInt(n[0]);
 int quan = Integer.parseInt(n[1]);
          

            if (id1 == id ) {
            } else {
                s1 = s1 + id1 + ":" + quan + "/";
            }
        }
        // s1=s1.replace(s1.substring(s1.length()-1), "").trim();
        if(s1.length()>2){
        s1 = s1.substring(0, s1.length() - 1);
        }
        Cart cart = new Cart(s1, lst);
        
        List<Item> list = cart.getItems();
        Cookie c = new Cookie("cart", s1);
        c.setMaxAge(60 * 60 *24);
        response.addCookie(c);
        request.setAttribute("cart", cart);
         request.setAttribute("data", lst);
       request.setAttribute("size", list.size());
        response.sendRedirect("gui");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         processRequest(request, response);
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
