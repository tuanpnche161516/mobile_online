/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package model;

import DAO.ProductDAO;
import controller.Cart;
import controller.Item;
import controller.Product;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author LEGION
 */
@WebServlet(name = "AddSub", urlPatterns = {"/sub"})
public class AddSub extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ProductDAO pd = new ProductDAO();
        List<Product> lst = pd.geProducts();
        Cookie arr[] = request.getCookies();
        String txt ="";
        for(Cookie o: arr){
            if(o.getName().equals("cart")){
                txt = txt + o.getValue();
                o.setMaxAge(0);
                response.addCookie(o);
            }
        }
        Cart cart=new Cart(txt, lst);
        String quantity_raw=request.getParameter("quantity");
        String id_raw=request.getParameter("id");
        int quantity=0,id;
        try{
            id=Integer.parseInt(id_raw);
            Product p = pd.geProductsID(id);
            int numStore=p.getProductQuantity();
            quantity=Integer.parseInt(quantity_raw);
            if(quantity==-1 &&(cart.getQuantityById(id)<=1)){
                cart.removeItem(id);
            }else{
                if(quantity==1 && cart.getQuantityById(id)>=numStore){
                    quantity=0;
                }
                int price=p.getProductPrice()*2;
                Item t=new Item(p, quantity, price);
                cart.addItem(t);
            }
        }catch(NumberFormatException e){
            
        }
        List<Item>items=cart.getItems();
        txt="";
        if(items.size()>0){
           txt=items.get(0).getProduct().getProductID()+":"+
                   items.get(0).getQuantity();
            for (int i = 1; i < items.size(); i++) {
                txt+="/"+items.get(i).getProduct().getProductID()+":"+
                        items.get(i).getQuantity();
            }
        }
        Cookie c=new Cookie("cart", txt);
        c.setMaxAge(2*24*60*60);
        response.addCookie(c);
        request.setAttribute("cart", cart);
         response.sendRedirect("gui");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
