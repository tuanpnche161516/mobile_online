import java.io.IOException;
import java.io.PrintWriter;
 
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
 
//@WebServlet("/readCookies")
public class ReadCookiesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public ReadCookiesServlet() {
    }
 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
         
        Cookie[] a = request.getCookies();
         
        if (a == null) {
            out.println("No cookie found");
         } 
        else {
          out.println("Number of cookies: " + a.length);
          String name, value;     
          for (Cookie x : a) {
             name = x.getName();
             value = x.getValue();
             out.println(name + " = " + value);
          }
        }
    }
 
}
