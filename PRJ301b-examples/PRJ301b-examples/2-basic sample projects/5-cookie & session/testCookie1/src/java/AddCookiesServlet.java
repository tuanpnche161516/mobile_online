import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
 
//@WebServlet("/addCookies")
public class AddCookiesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static int count = 0;
     
    public AddCookiesServlet() {
    }
 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
        String name, value;
        count++;
        if(count<10)
          name = "cookie_0" + count;
         else
          name = "cookie_" + count;  
        value = String.valueOf(System.currentTimeMillis());
        Cookie x = new Cookie(name, value);       
         
        response.addCookie(x);
         
        response.getWriter().println("A cookie has been created successfully!");
         
    }
 
}
