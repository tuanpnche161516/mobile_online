import java.io.IOException;
import java.io.PrintWriter;
 
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
 
@WebServlet("/delete_cookies")
public class DeleteCookiesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DeleteCookiesServlet() {
    }
 
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
        PrintWriter out = response.getWriter();
         
        Cookie[] a = request.getCookies();
        if (a != null) {
           for (Cookie x : a) {
              x.setMaxAge(0);
              response.addCookie(x);
          }
          out.println("All cookies have been deleted!");
         } 
         else {
           out.println("No cookie found");
        }
    }
 
}
