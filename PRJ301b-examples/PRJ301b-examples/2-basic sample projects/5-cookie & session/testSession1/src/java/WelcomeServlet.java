import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
public class WelcomeServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      response.setContentType("text/html;charset=UTF-8");
      PrintWriter out = response.getWriter();
      HttpSession sess=request.getSession(false);
      String xName=(String)sess.getAttribute("uname");
      String xPass=(String)sess.getAttribute("upass");
      out.print("Name: " + xName+"<p>Pass: " + xPass);
      out.close();
    }

}
