import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
public class LoginServlet extends HttpServlet {
   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       response.setContentType("text/html");

      HttpSession sess=request.getSession();
      PrintWriter out = response.getWriter();
      String userName, password;
      userName = request.getParameter("user");
      password = request.getParameter("pw");
      sess.setAttribute("uname",userName);
      sess.setAttribute("upass",password);
      out.print("<p>User name and password have been saved to session");
      request.getRequestDispatcher("index.html").include(request, response);
  }

}
