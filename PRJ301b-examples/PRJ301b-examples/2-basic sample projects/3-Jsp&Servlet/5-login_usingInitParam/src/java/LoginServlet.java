import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xUser,xPass,iUser,iPass;
        xUser = request.getParameter("user");
        xPass = request.getParameter("pass");
        iUser = this.getInitParameter("username");
        iPass = this.getInitParameter("password");
        if(xUser.equals(iUser) && xPass.equals(iPass)) {
           request.setAttribute("user", xUser);
           //request.setAttribute("pass", xPass);
           request.getRequestDispatcher("welcome.jsp").forward(request, response);
        }
        else {
           pr.print("<h4>Invalid username and/or password</h4>");
           request.getRequestDispatcher("index.html").include(request, response);
           pr.print("<h4>Please enter correct username and password</h4>");
        }
    }

}
