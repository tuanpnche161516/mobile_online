import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<h2> This is  doGet() function");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String s1 = request.getParameter("n1");
            String s2 = request.getParameter("n2");
            int k1 = Integer.parseInt(s1.trim());
            int k2 = Integer.parseInt(s2.trim());
            int k = k1 + k2;
            out.println("<h2>" + k1 + " + " + k2 + " = " + k  + "</h2>");
        }
    }

}
