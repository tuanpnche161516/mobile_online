import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class AddServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String s = "<html><head>";
        s += "<title>TODO supply a title</title>";
        s += "</head><body>";
        s += "<form action='add' method='POST'>";
        s += "<p>Number 1: <input type='text' name='n1' value=''>";
        s += "<p>Number 2: <input type='text' name='n2' value=''>";
        s += "<p>Sum: <input type='text' name='n3' value=''>";
        s += "<p><input type='submit' value='Sum'>";
        s += "</form></body></html>";
        pr.print(s);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String m1 = request.getParameter("n1");
        String m2 = request.getParameter("n2");
        int k1 = Integer.parseInt(m1.trim());
        int k2 = Integer.parseInt(m2.trim());
        int m3 = k1 + k2;
        String s = "<html><head>";
        s += "<title>TODO supply a title</title>";
        s += "</head><body>";
        s += "<form action='add' method='POST'>";
        s += "<p>Number 1: <input type='text' name='n1' value='" + m1 + "'>";
        s += "<p>Number 2: <input type='text' name='n2' value='" + m2 + "'>";
        s += "<p>Sum: <input type='text' name='n3' value='" + m3 + "'>";
        s += "<p><input type='submit' value='Sum'>";
        s += "</form></body></html>";
        pr.print(s);
    }

}
