import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class AddServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String x,y; int n1,n2,n3;
        x = request.getParameter("n1");
        y = request.getParameter("n2");
        n1 = Integer.parseInt(x.trim());
        n2 = Integer.parseInt(y.trim());
        n3 = n1+n2;
        request.setAttribute("m1", n1);
        request.setAttribute("m2", n2);
        request.setAttribute("m3", n3);
        request.getRequestDispatcher("add.jsp").forward(request, response);
    }

}
