<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <title>Calculate sum of 2 numbers</title>
  </head>
  <body>
      <%! Integer n1,n2,n3; %>    
      <%
         n1 = (Integer) request.getAttribute("n1");
         n2 = (Integer) request.getAttribute("n2");
         n3 = (Integer) request.getAttribute("n3");
      %>    
      <form action="add" method="POST">
          <p>Number 1: <input type="text" name="n1" value="<%= n1 %>">  
          <p>Number 2: <input type="text" name="n2" value="<%= n2 %>">  
          <p>Sum: <input type="text" name="n3" value="<%= n3 %>">
          <p><input type="submit" value="Calculate">    
      </form>    
  </body>
</html>
