import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class AddServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String m1 = request.getParameter("n1");
        String m2 = request.getParameter("n2");
        int n1 = Integer.parseInt(m1.trim());
        int n2 = Integer.parseInt(m2.trim());
        int n3 = n1 + n2;
        request.setAttribute("n1",n1);
        request.setAttribute("n2",n2);
        request.setAttribute("n3",n3);
        request.getRequestDispatcher("add.jsp").forward(request, response);
    }

}
