<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
  </head>

  <%! String sn1,sn2;int n1,n2,n3; %>
  <%
     sn1 = request.getParameter("n1").trim();
     sn2 = request.getParameter("n2").trim();
     n1 = Integer.parseInt(sn1);
     n2 = Integer.parseInt(sn2);
     n3 = n1 + n2;
  %>    
  <body>
    <form action="#" method="post">
      Number 1:
      <input type="text" name="n1" value="<%= n1 %>"/>    
      <p>Number 2:
      <input type="text" name="n2" value="<%= n2 %>"/>    
      <p>Sum:
      <input type="text" name="n3" value="<%= n3 %>"/>    
      <p><input type="Submit" value="Sum">
    </form>  
  </body>
</html>
