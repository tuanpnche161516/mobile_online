import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TestServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xName = request.getParameter("username");
        pr.print("Hello " + xName); // Insert before  
        request.getRequestDispatcher("/index.html").include(request, response);
        pr.print("Bye bye"); // Insert after 

    }


}
