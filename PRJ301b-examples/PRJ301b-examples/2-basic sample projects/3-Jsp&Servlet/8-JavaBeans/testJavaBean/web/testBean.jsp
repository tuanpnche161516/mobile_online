<%-- 
    Document   : testBean
    Created on : Dec 16, 2020, 8:31:30 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      <jsp:useBean id = "employees" class = "model.EmployeeBean"> 
         <jsp:setProperty name = "employees" property = "firstName" value = "Hoa"/>
         <jsp:setProperty name = "employees" property = "lastName" value = "La"/>
         <jsp:setProperty name = "employees" property = "startYear" value = "2005"/>
         <jsp:setProperty name = "employees" property = "payRate" value = "10.5"/>
      </jsp:useBean>
      <p>Employee First Name: 
         <jsp:getProperty name = "employees" property = "firstName"/>
      </p>
      
      <p>Employee Last Name: 
         <jsp:getProperty name = "employees" property = "lastName"/>
      </p>
      
      <p>Employee Start Year: 
         <jsp:getProperty name = "employees" property = "startYear"/>
      </p>
      
      <p>Employee Pay Rate: 
         <jsp:getProperty name = "employees" property = "payRate"/>
      </p>
      
    </body>
</html>
