package model;
import java.io.Serializable;
public class EmployeeBean implements Serializable {
   private String firstName; 
   private String lastName; 
   private int startYear;
   private double payRate; // the hourly pay rate

    public EmployeeBean() {
    }

    public EmployeeBean(String firstName, String lastName, int startYear, double payRate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.startYear = startYear;
        this.payRate = payRate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getStartYear() {
        return startYear;
    }

    public double getPayRate() {
        return payRate;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public void setPayRate(double payRate) {
        this.payRate = payRate;
    }
   
}
