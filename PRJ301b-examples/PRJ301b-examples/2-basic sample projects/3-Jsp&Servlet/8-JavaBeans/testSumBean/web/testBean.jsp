<%-- 
    Document   : testBean
    Created on : Dec 16, 2020, 8:31:30 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      <jsp:useBean id = "sum_bean" class = "model.SumBean"> 
          <jsp:setProperty name = "sum_bean" property = "num1"/>
          <jsp:setProperty name = "sum_bean" property = "num2"/>
      </jsp:useBean>
      <p>  
          <jsp:getProperty name = "sum_bean" property = "num1"/>
          +
          <jsp:getProperty name = "sum_bean" property = "num2"/>
          =
          <jsp:getProperty name = "sum_bean" property = "sum"/>
      </p>
          
    </body>
</html>
