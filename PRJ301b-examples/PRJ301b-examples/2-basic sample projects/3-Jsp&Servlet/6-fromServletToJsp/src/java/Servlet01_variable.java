import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
public class Servlet01_variable extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String x, y; x = "Hello";y="Good morning";
        request.setAttribute("x1", x);
        request.setAttribute("y1", y);
        request.getRequestDispatcher("jsp01_variable.jsp").forward(request, response);
    }

}
