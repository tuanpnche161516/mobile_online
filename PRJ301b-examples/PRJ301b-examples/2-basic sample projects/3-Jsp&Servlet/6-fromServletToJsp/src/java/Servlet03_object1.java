import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import model.*;
public class Servlet03_object1 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        User x = new User("hoa","123",1);
        request.setAttribute("y", x);
        request.getRequestDispatcher("jsp03_object1.jsp").forward(request, response);
    }

}
