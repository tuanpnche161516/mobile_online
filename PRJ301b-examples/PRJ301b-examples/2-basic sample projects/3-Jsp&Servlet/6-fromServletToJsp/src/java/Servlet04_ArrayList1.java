import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import model.*;
import java.util.*;
public class Servlet04_ArrayList1 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ArrayList<User> t = new ArrayList<User>();
        t.add(new User("hoa","123",1));
        t.add(new User("la","12",2));
        t.add(new User("canh","1234",3));
        request.setAttribute("h", t);
        request.getRequestDispatcher("jsp04_ArrayList1.jsp").forward(request, response);
    }

}
