package model;

public class User {
  String name, pass; int perm;

    public User(String name, String pass, int perm) {
        this.name = name;
        this.pass = pass;
        this.perm = perm;
    }

    public String getName() {
        return name;
    }

    public String getPass() {
        return pass;
    }

    public int getPerm() {
        return perm;
    }
  
}
