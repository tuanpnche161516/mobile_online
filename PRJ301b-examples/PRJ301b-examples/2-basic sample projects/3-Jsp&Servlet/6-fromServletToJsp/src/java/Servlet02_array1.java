import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
public class Servlet02_array1 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String [] a = {"hoa","la","canh"};
        request.setAttribute("b", a);
        request.getRequestDispatcher("jsp02_array1.jsp").forward(request, response);
    }

}
