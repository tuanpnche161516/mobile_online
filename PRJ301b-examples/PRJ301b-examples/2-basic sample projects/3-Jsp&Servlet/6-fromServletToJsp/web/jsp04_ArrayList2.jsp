<%-- User class must be in package --%>
<%@page import="model.User"%> 
<%@page import="java.util.*"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pass ArrayList</title>
        <style>
          th {background-color:yellow;}  
          td {text-align: center; vertical-align: middle;}  
        </style>
    </head>
    <body>
      <h3>List of users:
       <table border="1" cellpadding="2" cellspacing="2">
        <tr>
          <th>Username</th>
          <th>Password</th>
          <th>Permission</th>
         </tr>
         <%List<User> r = (List<User>) request.getAttribute("h");%>
         <%int n,i; n = r.size(); User x; %>
         <% for(i=0;i<n;i++) { %>
         <% x = r.get(i);%>
          <tr>
           <td><%=x.getName()%></td>
           <td><%=x.getPass()%></td>
           <td><%=x.getPerm()%></td>
          </tr>
         <% } %>
       </table>
      </h3>
    </body>
</html>
 