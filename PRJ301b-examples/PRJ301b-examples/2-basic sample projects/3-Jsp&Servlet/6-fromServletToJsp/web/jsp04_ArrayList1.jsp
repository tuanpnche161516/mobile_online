<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pass ArrayList</title>
        <style>
          th {background-color:yellow;}  
          td {text-align: center; vertical-align: middle;}  
        </style>
    </head>
    <body>
      <h3>List of users:
       <table border="1" cellpadding="2" cellspacing="2">
        <tr>
          <th>Username</th>
          <th>Password</th>
          <th>Permission</th>
         </tr>
         <c:forEach var="x" items="${h}">
          <tr>
           <td>${x.getName()}</td>
           <td>${x.getPass()}</td>
           <td>${x.getPerm()}</td>
          </tr>
         </c:forEach>
       </table>
      </h3>
    </body>
</html>
 