<%-- 
 The imported class must be included in a package,
 e.g. cannot write statement like: <%@page import="User"%>
--%>

<%@page import="model.User"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pass an object</title>
    </head>
    <body>
        <h2>Information about a user:
        <%User z = (User) request.getAttribute("y"); %>
            <p> Username: <%= z.getName() %>
            <br> Password: <%= z.getPass() %>  
            <br> Permission: <%= z.getPerm() %>
        </h2>
    </body>
</html>
 