<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pass an object</title>
    </head>
    <body>
        <h2>Information about a user:
            <p> Username: ${y.name}
            <br> Password: ${y.pass}  
            <br> Permission: ${y.perm}  
        </h2>
    </body>
</html>
 