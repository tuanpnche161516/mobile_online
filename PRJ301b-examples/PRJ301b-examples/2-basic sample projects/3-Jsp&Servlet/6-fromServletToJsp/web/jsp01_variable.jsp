<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pass a variable</title>
    </head>
    <body>
        <h2>Pass variables from servlet to jsp:</h2>
        <p>
        <% String x2 = (String)request.getAttribute("x1"); %>
        <% String y2 = (String)request.getAttribute("y1"); %>
        <h2>Display using JSP Scriptlet (&lt;% ... %&gt;) (ok) </h2>
        <h3>x = <%= x2 %></h3>
        <h3>y = <%= y2 %></h3>
        <h3>Display using JSTL (ok) :</h3>
        <h3> x = ${x1} </h3>
        <h3> y = ${y1} </h3>
        <h3>Display using JSTL (not ok) :</h3>
        <h3> x = ${x2} </h3>
        <h3> y = ${y2} </h3>
    </body>
</html>
