import java.io.*;

import jakarta.servlet.*;
import jakarta.servlet.http.*;


public class HelloServlet extends HttpServlet {
   public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
      response.setContentType("text/html");

      PrintWriter pr = response.getWriter();

      String xUserid = request.getParameter("userid");
      request.setAttribute("userid", xUserid);

      request.getRequestDispatcher("hello.jsp").forward(request, response);
   }

}
