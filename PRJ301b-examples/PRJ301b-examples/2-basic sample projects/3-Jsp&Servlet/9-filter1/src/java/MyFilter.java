import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.*;

public class MyFilter implements Filter{

   public void init(FilterConfig arg0) throws ServletException {}
	
   public void doFilter(ServletRequest req, ServletResponse resp,
 	   FilterChain chain) throws IOException, ServletException {	

      PrintWriter pr = resp.getWriter();

      pr.print("<h3>filter is invoked before</h3>");

      String x = req.getParameter("userid");
      if(Character.isAlphabetic(x.charAt(0)))
         chain.doFilter(req, resp);//sends request to next resource
	  else
           pr.print("<h3> The first character of userid must be letter </h3>");

	pr.print("<h3>filter is invoked after</h3>");
			
	}

   public void destroy() {}

 }
