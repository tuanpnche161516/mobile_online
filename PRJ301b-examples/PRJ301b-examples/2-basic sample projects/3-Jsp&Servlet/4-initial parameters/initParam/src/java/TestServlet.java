import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String xUser = this.getInitParameter("username");
        String xPass = this.getInitParameter("password");
        out.println("<h2> Test servlet parameters: </h2>");
        out.println("<h2> username: " + xUser + "</h2>");
        out.println("<h2> password: " + xPass + "</h2>");
    }

}
