<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test JSTL with relative path</title>
    </head>
    <%! int [] a = {7,3,5,9,2,8};%>
    <% request.setAttribute("b",a); %>
    <body>
       <h2>The values of the array a are:
       <p>
       <c:forEach var="x" items="${b}">
        ${x}&nbsp;
       </c:forEach>
      </h2>     
    </body>
</html>
 