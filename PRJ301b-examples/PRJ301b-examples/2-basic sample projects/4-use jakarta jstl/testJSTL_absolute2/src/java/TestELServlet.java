import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TestELServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String [] a = {"Hoa","La","Canh","Cay"};
        request.setAttribute("b", a);
        request.getRequestDispatcher("test.jsp").forward(request, response);
    }

}
