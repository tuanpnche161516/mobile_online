<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %> <%--KHong can ket noi internet?--%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
  </head>
  <body>
    <h2>Test JSTL 2</h2>
    <p>
    <%
     int [] a = {2,7,4,5};    
    %>
    EL&JSTL khong hieu cac bien khai bao trong JSP scriptlets (&lt;\%...\%&gt;):
    <p>
   <c:forEach var="x" items="${a}">
        <br> ${x}
   </c:forEach>
   
    Neu dung setAttribute vao bien pham vi request hoac session thi hieu duoc 
    <%
     request.setAttribute("b", a);    
    %>        
   <c:forEach var="x" items="${b}"> <!-- Co hien thi -->
        <br> ${x}
   </c:forEach>
        
  </body>
</html>
