import java.sql.*;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
      StudentDAO t = new StudentDAO();
      t.display();
      String xRollno, xName; float xMark;
      System.out.println("\nEnter the student to be updated:");
      System.out.print("Rollno: ");
      Scanner u = new Scanner(System.in);
      xRollno = u.nextLine();
      Student x  = t.search(xRollno);
      if(x == null) {
        System.out.println("The student with rollno " + xRollno + " is not found.");
        return;
       }
      System.out.print("Name: ");
      xName = u.nextLine();
      System.out.print("Mark: ");
      xMark = u.nextFloat();
      u.nextLine();
      x = new Student(xRollno,xName,xMark);
      t.updateByRollno(x);
      t.display();
    }
      
  }
