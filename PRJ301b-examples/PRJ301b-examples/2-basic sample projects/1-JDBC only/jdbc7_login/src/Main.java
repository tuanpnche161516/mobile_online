
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner u = new Scanner(System.in);
        String xName, xPass;
        User x = null;
        System.out.println("Enter username and password:");
        System.out.print("Name: ");
        xName = u.nextLine(); // hoa
        System.out.print("Password: ");
        xPass = u.nextLine(); // 123
        UserDAO t = new UserDAO();
        x = t.search(xName, xPass);
        if(x==null) {
          System.out.println("\nWrong username and/or password.");
        }
        else {
         System.out.println("\nUser found:");
         System.out.println("Name: " + x.getName());
         System.out.println("Password: " + x.getPass());
         System.out.println("Role: " + x.getRole());
        } 
        System.out.println();
        t.display();
    }
    
}
