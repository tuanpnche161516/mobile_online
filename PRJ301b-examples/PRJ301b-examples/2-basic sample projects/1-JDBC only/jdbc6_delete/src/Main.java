import java.sql.*;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
      StudentDAO t = new StudentDAO();
      t.display();
      String xRollno, xName; float xMark;
      System.out.println("\nEnter the student to be deleted:");
      System.out.print("Rollno: ");
      Scanner u = new Scanner(System.in);
      xRollno = u.nextLine();
      Student x  = t.search(xRollno);
      if(x == null) {
        System.out.println("The student with rollno " + xRollno + " is not found.");
        return;
       }
      System.out.println("\nInformation about the student to be deleted :");
      System.out.println("Rollno: " + x.getRollno());
      System.out.println("Name: " + x.getName());
      System.out.println("Mark: " + x.getMark());
      System.out.print("Are you sure to delete this student? (Y/N) ");
      String v = u.nextLine();
      char c = v.charAt(0);
      if(Character.toUpperCase(c) == 'Y' && v.length()==1) {
         t.deleteByRollno(xRollno);
       }  

      t.display();
    }
      
  }
