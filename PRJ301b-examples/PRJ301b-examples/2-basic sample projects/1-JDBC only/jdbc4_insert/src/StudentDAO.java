import java.sql.*;
public class StudentDAO extends MyDAO {

   public void display() {
     xSql = "select * from student";
     try {
      ps = con.prepareStatement(xSql);
      rs = ps.executeQuery();
      String xRollno, xName; float xMark;
      System.out.println("List of students:");
      while(rs.next()) {
         xRollno = rs.getString(1);
         xName = rs.getString(2);
         xMark = rs.getFloat(3);
         System.out.println(xRollno + ", " + xName + ", " + xMark);
       }
       rs.close();        
      }
      catch(Exception e) {
        e.printStackTrace();
      }       
     }
   public Student search(String xRollno) {
      xSql = "select * from Student where rollno = ? ";
      Student x=null;
      try {
        ps = con.prepareStatement(xSql);
        ps.setString(1, xRollno);
        rs = ps.executeQuery();
        /* The cursor on the rs after this statement is in the BOF area, i.e. it is before the first record.
         Thus the first rs.next() statement moves the cursor to the first record
        */
       String xName=null; float xMark; 
       if(rs.next())
          xName=rs.getString("name") ;
          xMark=rs.getFloat("mark");
          x = new Student(xRollno,xName,xMark);
        rs.close();        
       }
       catch(Exception e) {
      }
     return(x); 
   } 
  public void insert(Student x) {
     xSql = "insert into Student (Rollno, name, mark) values (?,?,?)";
     try {
       ps = con.prepareStatement(xSql);
       ps.setString(1, x.getRollno());
       ps.setString(2, x.getName());
       ps.setFloat(3, x.getMark());
       ps.executeUpdate();
      } 
       catch(Exception e) {}  
  }     
}
