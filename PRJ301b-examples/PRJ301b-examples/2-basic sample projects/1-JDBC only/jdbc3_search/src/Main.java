import java.sql.*;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
      StudentDAO t = new StudentDAO();
      t.display();
      String xRollno;
      Scanner u = new Scanner(System.in);
      System.out.print("Enter rollno to be searched: ");
      xRollno = u.nextLine();
      Student x = t.search(xRollno);
       if(x!=null) { 
          System.out.println("The student with rollno " + xRollno + " is found:");
          System.out.println(x);
         } 
         else
          System.out.println("The student with rollno " + xRollno + " is not found.");
    }
      
  }
