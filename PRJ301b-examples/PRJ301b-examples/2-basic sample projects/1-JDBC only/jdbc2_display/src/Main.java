import java.sql.*;
public class Main {

    public static void main(String[] args) {
       String xServer = "localhost";
       String xPort = "1433";
       String xDbName = "studentDB";
       String xUrl = "jdbc:sqlserver://" + xServer + ":" + xPort + ";databaseName = " + xDbName;
    
       String xUser = "sa";
       String xPW = "sa";
       Connection con = null;
       PreparedStatement ps;
       ResultSet rs;
       String xSql = "select * from student";
       String xRollno, xName; float xMark;
       try {
         Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
         con = DriverManager.getConnection(xUrl,xUser,xPW);
         System.out.println("Connection successful.");
         ps = con.prepareStatement(xSql);
         rs = ps.executeQuery();
         while(rs.next()) {
            xRollno = rs.getString("rollno");
            xName = rs.getString("name");
            xMark = rs.getFloat("mark");
            System.out.println(xRollno + " " + xName + " " + xMark);
         }
        rs.close();
        ps.close();
        con.close();
       }
       catch(Exception e) {
         System.out.println("Connection failed.");
       }

    }
    
}
