package model;
import java.util.*;
import java.sql.*;

public class StudentDAO extends MyDAO {

  public List<Student> getStudents() {
    List<Student> t = new ArrayList<>();
    xSql = "select * from Student";
    try {
      ps = con.prepareStatement(xSql);
      rs = ps.executeQuery();
      String xRollno,xName; float xMark;
      Student x;
      while(rs.next()) {
        xRollno = rs.getString("rollno");  
        xName = rs.getString("name");  
        xMark = rs.getFloat("mark");
        x = new Student(xRollno,xName,xMark);
        t.add(x);
      }
      rs.close();
      ps.close();
     }
     catch(Exception e) {
        e.printStackTrace();
     }
    return(t);
  }
  public Student getStudent(String xRollno) {
     Student x = null;
     String xName; float xMark;
     xSql = "select * from Student where rollno=?"; 
     try {
      ps = con.prepareStatement(xSql);
      ps.setString(1, xRollno);
      rs = ps.executeQuery();
      if(rs.next()) {
        xName = rs.getString("name");
        xMark = rs.getFloat("mark");
        x = new Student(xRollno,xName,xMark);
      }
      rs.close();
      ps.close();
     }     
     catch(Exception e) {
        e.printStackTrace();
     }
     return(x);
  }
  public void delete(String xRollno) {
     xSql = "delete from Student where rollno=?";
     try {
        ps = con.prepareStatement(xSql);
        ps.setString(1, xRollno);
        ps.executeUpdate();
        //con.commit();
        ps.close();
     }
     catch(Exception e) {
        e.printStackTrace();
     }
  }
  public void insert(Student x) {
     xSql = "insert into Student values (?,?,?)"; 
     try {
      ps = con.prepareStatement(xSql);
      ps.setString(1, x.getRollno());
      ps.setString(2, x.getName());
      ps.setFloat(3, x.getMark());
      ps.executeUpdate();
      ps.close();
     }     
     catch(Exception e) {
        e.printStackTrace();
     }
  }
  public void update(String xRollno, Student x) {
     xSql = "update Student set name=?, mark=? where rollno=?";
     try {      
        ps = con.prepareStatement(xSql);
        ps.setString(1, x.getName());
        ps.setFloat(2, x.getMark());
        ps.setString(3, xRollno);
        ps.executeUpdate();
        ps.close();
     }
      catch(Exception e) {
        e.printStackTrace();
      }
     return;
  }
}
