package controller;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.util.*;
import model.*;

//import jakarta.servlet.annotation.WebServlet;
//@WebServlet(name = "TestServlet", urlPatterns = {"/test"})
public class SearchServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xRollno = request.getParameter("rollno");
        StudentDAO u = new StudentDAO();
        Student x = u.getStudent(xRollno);
        if(x==null) {
           pr.println("<h2> A student is not found</h2>");
           request.getRequestDispatcher("search.html").include(request, response);
        }
        else {
           request.setAttribute("x", x);
           request.getRequestDispatcher("search.jsp").forward(request, response);
        }

    }
}
