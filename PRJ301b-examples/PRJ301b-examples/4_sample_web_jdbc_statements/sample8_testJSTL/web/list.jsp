<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.Student" %>
<%@page import = "java.util.*" %>
<!DOCTYPE html>
<%
  //List<Student> lst = (List<Student>) request.getAttribute("lst");
  // If you use JSP scriptlet, the above statement is necessary. 
%>    
<html>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <title>List of students</title>
  </head>
  <body>
    <h2> List of students </h2>
    <table border="1">
      <tr>
        <td> Rollno </td>
        <td> Name </td>
        <td> Mark </td>
        <td colspan="3" align="center"> <a href="insert.html"> Insert </a> </td>
      </tr>
      <c:forEach var="x" items="${lst}">  
        <tr>  
          <td> ${x.rollno} </td>
          <td> ${x.name} </td>
          <td> ${x.mark} </td>
          <td>
            <c:choose>
              <c:when test="${empty x}">
                no data
              </c:when>  
              <c:when test="${x.mark<5}">
                failed  
              </c:when>  
              <c:when test="${x.mark<7}">
                average  
              </c:when>  
              <c:when test="${x.mark<9}">
                good  
              </c:when>  
              <c:otherwise>
                excellent  
              </c:otherwise>  
            </c:choose>                 
          </td>
          <td><a href="update?rollno=${x.rollno}">  Edit </a> </td>
          <td><a href="delete?rollno=${x.rollno}">  Delete </a> </td>
        </tr>  
      </c:forEach>  
    </table>     
    <p><button onclick='window.history.go(-1);'>Back to previous page</button>
    <p><a href="index.html">Back to homepage</a>
  </body>
</html>
