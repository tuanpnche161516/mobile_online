
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<!DOCTYPE html>
<%
  ProvinceDAO u = new ProvinceDAO();
  List<Province> lst = u.getProList();
%>  
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert a new worker</title>
  </head>
  <body>
    <h3>Insert a new worker</h3>
    <form action="insertWorker" method="POST">
       <p>Id: <input type="text" name="id" value=""/>
        
       <p>Name: <input type="text" name="name" value="Hoa"/>

       <p>Gender:
       <input type="radio" name="gender" value="male" /> Male
       <input type="radio" name="gender" value="female" checked="checked" /> Female
       <p> <input type="checkbox" name="married" value="married" checked="checked" /> Married
       
       <p>Dob: <input type="date" name="dob" value="1995-12-25" />
           
       <p>Province: 
           <select name="pro_id">
              <% for(Province x: lst) { %>
               <option value="<%= x.getPro_id() %>"> <%= x.getPro_name() %> </option>
              <% } %>
            </select>
            <p>
       <p>Income: <input type="text" name="income" value=""/>
       <p><input type="submit" value="Insert"> 
    </form>  
    </body>
</html>
