<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "java.util.*" %>
<!DOCTYPE html>
<%
  WorkerDAO u = new WorkerDAO();
  List<Worker> lst = u.getWorkerList();
  //request.setAttribute("lst", lst);
  //If you use JSTL, the above statement is necessary
%>  
<html>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <title>List of workers</title>
     <style type="text/css">
        a {text-decoration:none;} 
     </style>
  </head>
  <body>
    <h2> List of workers </h2>
    <table border="1">
      <tr>
        <td> Id </td>
        <td> Name </td>
        <td> Gender </td>
        <td> Dob </td>
        <td> Pro_id </td>
        <td> Income </td>
        <td>  </td>
      </tr>
       <% for(Worker x : lst) { %>
       <tr>
         <td><%= x.getId() %></td>
         <td><%= x.getName() %></td>
         <td><%= x.isGender()? "Male":"Female" %></td>
         <td> <%= x.getDob() %></td>
         <td><%= x.getPro_id() %></td>
         <td><%= x.getIncome() %></td>
        <td> <a href="deleteWorker?id=<%= x.getId() %>"> Delete  </td>  
        </tr>
        <%}%>
    </table>     
    <p><button onclick='window.history.go(-1);'>Back to previous page</button>
    <p><a href="index.html">Back to homepage</a>
  </body>
</html>
