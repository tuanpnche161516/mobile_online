<%-- 
    Document   : person
    Created on : Mar 20, 2021, 8:02:14 AM
    Author     : hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Create a new person</title>
  </head>
  <body>
    <h3>Create a new person</h3>
    <form action="person" method="POST">
       <p>Id: <input type="text" name="id" value="A1" />
        
       <p>Name: <input type="text" name="name" value="Vi Thi Hoa" />

       <p>Dob: <input type="date" name="dob" value="1995-12-25" />
       <p>Gender:
       <input type="radio" name="gender" value="male" /> Male
       <input type="radio" name="gender" value="female" checked="checked" /> Female
       <p> <input type="checkbox" name="married" value="married" checked="checked" /> Married
           
       <p><input type="submit" value="Display"> 
    </form>  
    <p><button onclick='window.history.go(-1);'>Back to previous page</button>
    <p><a href="index.html">Back to homepage</a>
    </body>
</html>
