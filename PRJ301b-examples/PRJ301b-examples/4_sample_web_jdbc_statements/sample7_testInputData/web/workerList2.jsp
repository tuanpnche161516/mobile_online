<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import = "model.*" %>
<%@page import = "java.util.*" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%
  WorkerDAO u = new WorkerDAO();
  List<Worker> lst = u.getWorkerList();
  request.setAttribute("lst", lst); // without this statement JSTL cannot work
%>  
<html>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <title>List of workers</title>
     <style type="text/css">
        a {text-decoration:none;} 
     </style>
  </head>
  <body>
    <h2> List of workers </h2>
    <table border="1">
      <tr>
        <td> Id </td>
        <td> Name </td>
        <td> Gender </td>
        <td> Dob </td>
        <td> Pro_id </td>
        <td> Income </td>
        <td>  </td>
      </tr>
      <c:forEach var="x" items="${lst}"> 
      <tr>
        <td> ${x.id} </td>
        <td> ${x.name} </td>
        <td>
          <c:choose>
             <c:when test="${x.gender}">
                Male
             </c:when>
             <c:otherwise>
                Female 
             </c:otherwise>
          </c:choose>
        </td>
        <td> ${x.dob} </td>
        <td> ${x.pro_id}</td>
        <td> ${x.income} </td>
        <td> <a href="deleteWorker?id=${x.id}"> Delete  </td>  
       </tr>
       </c:forEach>
    </table>     
    <p><button onclick='window.history.go(-1);'>Back to previous page</button>
    <p><a href="index.html">Back to homepage</a>
  </body>
</html>
