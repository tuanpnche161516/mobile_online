package model;
import java.util.List;
import java.util.ArrayList;
import java.util.Date; // Date and time

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class WorkerDAO extends MyDAO {

  public List<Worker> getWorkerList() { 
    List<Worker> t = new ArrayList<Worker>();
    xSql = "select * from Worker";
    try {
      ps = con.prepareStatement(xSql);
      rs = ps.executeQuery();
      String xId,xName; boolean xGender; Date xDob; Province xPro; String xPro_id, xPro_name; int xIncome;
      Worker x;
      while(rs.next()) {
        xId = rs.getString("id");  
        xName = rs.getString("name");  
        xGender = rs.getBoolean("gender");
        xDob = rs.getDate("dob");
        xPro_id = rs.getString("pro_id");
        xIncome = rs.getInt("income");
        x = new Worker(xId,xName,xGender,xDob,xPro_id,xIncome);
        t.add(x);
      }
      rs.close();
      ps.close();
     }
     catch(Exception e) {
        e.printStackTrace();
     }
    return(t);
  }

  public Worker getWorker(String xId) {
    if(xId==null || xId.trim().equals("")) return(null);  
    Worker x = null;
    xSql = "select * from Worker where id = ?";
    try {
      ps = con.prepareStatement(xSql);
      ps.setString(1, xId);
      rs = ps.executeQuery();
      String xName; boolean xGender; Date xDob; String xPro_id; int xIncome;
      if(rs.next()) {
        xId = rs.getString("id");  
        xName = rs.getString("name");  
        xGender = rs.getBoolean("gender");
        xDob = rs.getDate("dob");
        xPro_id = rs.getString("pro_id");
        xIncome = rs.getInt("income");
        x = new Worker(xId,xName,xGender,xDob,xPro_id,xIncome);
      }
      rs.close();
      ps.close();
     }
     catch(Exception e) {
        e.printStackTrace();
     }
     return(x);
  }

    public String insert(Worker x) {
     xSql = "insert into Worker (id,name,gender,dob,pro_id,income) values (?,?,?,?,?,?)"; 
     java.sql.Date xDob = new java.sql.Date(x.getDob().getTime()); 
     try {
      ps = con.prepareStatement(xSql);
      ps.setString(1, x.getId());
      ps.setString(2, x.getName());
      ps.setBoolean(3, x.isGender());
      ps.setDate(4, xDob);
      ps.setString(5, x.getPro_id());
      ps.setInt(6, x.getIncome());
      ps.executeUpdate();
      ps.close();
     }     
     catch(Exception e) {
        return(e.getMessage());
     }
     return("Ok");
  }


  public void updateById(Worker x) {
     xSql = "update Worker set name=?, gender=?,dob=?,pro_id=?,income=? where id=?";
     java.sql.Date xDob = new java.sql.Date(x.getDob().getTime()); 
     try {      
        ps = con.prepareStatement(xSql);
        ps.setString(1, x.getName());
        ps.setBoolean(2, x.isGender());
        ps.setDate(3, xDob);
        ps.setString(4, x.getPro_id());
        ps.setInt(5, x.getIncome());
        ps.setString(6, x.getId());
        ps.executeUpdate();
        ps.close();
     }
      catch(Exception e) {
        e.printStackTrace();
      }
     return;
  }

  public void deleteById(String xId) {
     xSql = "delete from Worker where id=?";
     try {
        ps = con.prepareStatement(xSql);
        ps.setString(1, xId);
        ps.executeUpdate();
        ps.close();
     }
     catch(Exception e) {
        e.printStackTrace();
     }
  }

}
