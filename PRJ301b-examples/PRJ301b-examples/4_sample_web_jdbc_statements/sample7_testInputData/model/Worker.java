package model;
import java.util.Date;

public class Worker {
  private String id, name;
  private boolean gender;
  private Date dob;
  private String pro_id;
  private int income;

    public Worker() {
    }

    public Worker(String id, String name, boolean gender, Date dob, String pro_id, int income) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.dob = dob;
        this.pro_id = pro_id;
        this.income = income;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isGender() {
        return gender;
    }

    public Date getDob() {
        return dob;
    }

    public String getPro_id() {
        return pro_id;
    }

    public int getIncome() {
        return income;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public void setIncome(int income) {
        this.income = income;
    }
  
}
