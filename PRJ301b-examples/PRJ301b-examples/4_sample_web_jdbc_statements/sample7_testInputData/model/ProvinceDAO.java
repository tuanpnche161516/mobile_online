package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProvinceDAO extends MyDAO {

  public List<Province> getProList() {
    List<Province> t = new ArrayList<>();
    xSql = "select * from Province";
    try {
      ps = con.prepareStatement(xSql);
      rs = ps.executeQuery();
      String xPro_id,xPro_name;
      Province x;
      while(rs.next()) {
        xPro_id = rs.getString("pro_id");  
        xPro_name = rs.getString("Pro_name");  
        x = new Province(xPro_id,xPro_name);
        t.add(x);
      }
      rs.close();
      ps.close();
     }
     catch(Exception e) {
        e.printStackTrace();
     }
    return(t);
  }
  
  public Province getProvince(String xPro_id) {
    if(xPro_id==null || xPro_id.trim().equals("")) return(null);  
     Province x = null;
     String xPro_name = null;
     xSql = "select * from Province where pro_id=?";
     try {
       ps = con.prepareStatement(xSql);
       ps.setString(1,xPro_id);
       rs = ps.executeQuery();
        /* The cursor on the rs after this statement is in the BOF area, i.e. it is before the first record.
         Thus the first rs.next() statement moves the cursor to the first record
        */
       if(rs.next()) {
          xPro_name = rs.getString("pro_name");
          x = new Province(xPro_id,xPro_name);
        } 
        rs.close();
        ps.close();
       }
       catch(Exception e) {
        e.printStackTrace();
      }
     return(x); 
   } 

  public void insert(Province x) {
     xSql = "insert into Province values (?,?)"; 
     try {
      ps = con.prepareStatement(xSql);
      ps.setString(1, x.getPro_id());
      ps.setString(2, x.getPro_name());
      ps.executeUpdate();
      ps.close();
     }     
     catch(Exception e) {
        e.printStackTrace();
     }
  }

  public void updateById(Province x) {
     xSql = "update Province set pro_name=? where pro_id=?";
     try {      
        ps = con.prepareStatement(xSql);
        ps.setString(1, x.getPro_name());
        ps.setString(2, x.getPro_id());
        ps.executeUpdate();
        ps.close();
     }
      catch(Exception e) {
        e.printStackTrace();
      }
     return;
  }

  public void deleteById(String xPro_id) {
     xSql = "delete from Province where pro_id=?";
     try {
        ps = con.prepareStatement(xSql);
        ps.setString(1, xPro_id);
        ps.executeUpdate();
        ps.close();
     }
     catch(Exception e) {
        e.printStackTrace();
     }
  }
  
}
