package controller;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

import model.*;
import java.util.Date;
import java.text.SimpleDateFormat;

public class InsertWorkerServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xId,xName,sGender,sDob,xPro_id,sIncome;
        boolean xGender; Date xDob = null; int xIncome=0;
        Province xPro;
        xId = request.getParameter("id").trim();
        if(xId.length()==0) {
           pr.print("<h3> The id must not be empty!");
           request.getRequestDispatcher("insertWorker.jsp").include(request, response);
           return;
         }
        WorkerDAO u = new WorkerDAO();
        Worker x = u.getWorker(xId);
        if(x != null) {
           pr.print("<h3> The id " + xId + " already exists!");
           request.getRequestDispatcher("insertWorker.jsp").include(request, response);
           return;
        }
        xName = request.getParameter("name").trim();
        if(xName.length()==0) {
           pr.print("<h3> The name cannot be empty!");
           request.getRequestDispatcher("insertWorker.jsp").include(request, response);
           return;
         }
        sGender = request.getParameter("gender");
        if(sGender==null) {
           pr.print("<h3> The gender cannot be empty!");
           request.getRequestDispatcher("insertWorker.jsp").include(request, response);
           return;
        }
        if(sGender.equals("male"))
            xGender = true;
          else
            xGender = false;
        sDob = request.getParameter("dob");
        try {
          xDob = new SimpleDateFormat("yyyy-MM-dd").parse(sDob);           
        } catch(Exception e) {}
        xPro_id = request.getParameter("pro_id").trim();
        ProvinceDAO v = new ProvinceDAO();
        xPro = v.getProvince(xPro_id);
        sIncome = request.getParameter("income").trim();
        if(sIncome.length()==0)
           xIncome = 0;
          else
           xIncome = Integer.parseInt(sIncome);
        x = new Worker(xId,xName,xGender,xDob,xPro_id,xIncome);
        u.insert(x);
        response.sendRedirect("workerList.jsp");
    }
}
