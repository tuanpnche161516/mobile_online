package controller;
import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class PersonServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xId,xName,xGender,xDob,xMarried,xPro,xLangs;
        xId = request.getParameter("id");
        xName = request.getParameter("name");
        xGender = request.getParameter("gender");
        xDob = request.getParameter("dob");
        xMarried = request.getParameter("married");
        xPro = request.getParameter("pro");
        String [] a = request.getParameterValues("lang");
        xLangs = "";
        int i;
        if(a==null)
            xLangs = "No language";
        else {
          xLangs = a[0];    
        for(i=1;i<a.length;i++)
            xLangs += ", " + a[i];
        }
        
        String str = "<html>";
         str += "<p> Id: " + xId;
         str += "<p> Name: " + xName;
         str += "<p> Gender: " + xGender;
         str += "<p> Dob: " + xDob;
         str += "<p> Married: " + xMarried;
         str += "<p> Province: " + xPro;
         str += "<p> Language(s): " + xLangs;
         
         str += "<p>";
         str += "<p>Important notes:";
         str += "<br>blank text -> blank text";        
         str += "<br>unselected radio or checkbox -> null";        
         str += "<br>selected radio or checkbox -> String value";        
         str += "</html>";
        pr.print(str);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xId,xName,sDob;
        boolean isMale,isMarried;
        xId = request.getParameter("id");
        xName = request.getParameter("name");
        isMale = request.getParameter("gender").trim().equals("male");
        isMarried = request.getParameter("married") != null;
        sDob = request.getParameter("dob");
        String str = "<html>";
        str += "<table border='1'>";
        str += "<tr>";
        str += "<td> Id </td>";
        str += "<td>" + xId + "</td>";
        str += "</tr>";
        
        str += "<tr>";
        str += "<td> Name: </td>";
        if(isMale)
         str += "<td>" + xName + " (Mr)</td>";
         else
         str += "<td>" + xName + " (Ms)</td>";
        str += "</tr>";
        
        str += "<tr>";
        str += "<td> DOB: </td>";
        str += "<td>" + sDob + "</td>";
        str += "</tr>";

        str += "<tr>";
        str += "<td> Married: </td>";
        if(isMarried)
         str += "<td> Yes </td>";
         else
         str += "<td> No </td>";
        str += "</tr>";

        str += "</table>";
        str += "</html>";
        pr.print(str);
    }

}
