package controller;
import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TestServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xFirstName = request.getParameter("firstName");
       
        String str = "<html>";
         str += "<body>";
         str += "<h2>";
         str += "<p> First name: " + xFirstName;
         str += "</h2>";
         str += "</body>";
         str += "</html>";
        pr.print(str);
    }

}
