package controller;
import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import model.*;
import java.util.Date;
import java.text.SimpleDateFormat;

public class WorkerServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        String xId,xName,sGender,sMarried,sDob,xPro_id,xPro_name,sIncome;
        boolean xGender,xMarried; Date xDob; int xIncome;
        xId = request.getParameter("id");
        xName = request.getParameter("name");
        xGender = request.getParameter("gender").trim().equals("male");
        xMarried = request.getParameter("married") != null;
        sGender = xGender?"male":"female";
        sMarried = xMarried?"married":"single";
        sDob = request.getParameter("dob");
        xPro_id = request.getParameter("pro_id");
        ProvinceDAO u = new ProvinceDAO();
        Province x = u.getProvince(xPro_id);
        if(x == null) 
            xPro_name = "Unknown";
           else
            xPro_name = x.getPro_name();
        sIncome = request.getParameter("income").trim();
        if(sIncome.equals(""))
           xIncome = 0;
          else
           xIncome = Integer.parseInt(sIncome);
        String [] a = request.getParameterValues("provinces");
        String comeProvinces = "";
        int i;
        if(a==null || a.length==0)
            comeProvinces = "No province";
        else {
        comeProvinces = a[0];    
        for(i=1;i<a.length;i++)
            comeProvinces += ", " + a[i];
        }
        String ss = "";
        ss = "Id: " + xId + "<br>";
        ss += "Name: " + xName +  "<br>";
        ss += "Gender: " + sGender +  "<br>";
        ss += "Marrital status: " + sMarried+  "<br>";
        ss += "Dob: " + sDob +  "<br>";
        ss += "Province: " + xPro_name +  "<br>";
        ss += "Provinces have come: ";
        ss += comeProvinces + "<br>";
        ss += "Imcome: " + xIncome +  "<p>";
        pr.print(ss);
    }
}
