import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TestServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String xUsername = this.getInitParameter("username");
        String xPassword = this.getInitParameter("password");
        out.print("<H2> Username: " + xUsername + "</H2>" );
        out.print("<H2> Password: " + xPassword + "</H2>" );

    }

}

