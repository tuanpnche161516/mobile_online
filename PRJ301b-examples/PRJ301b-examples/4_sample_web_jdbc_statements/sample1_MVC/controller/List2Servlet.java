package controller;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.WebServlet;
import model.*;
import java.util.*;

@WebServlet(urlPatterns = {"/list2"})
public class List2Servlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();
        StudentDAO u = new StudentDAO();
        List<Student> lst = u.getStudents();
        request.setAttribute("lst", lst);
        request.getRequestDispatcher("list2.jsp").forward(request, response);

    }

}
