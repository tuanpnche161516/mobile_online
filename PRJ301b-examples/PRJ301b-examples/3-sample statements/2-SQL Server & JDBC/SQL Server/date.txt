SELECT convert(nvarchar(10),getdate(),120) /* 2020-10-21 */
SELECT convert(nvarchar(10),getdate(),103) /* 21/10/2020 */
SELECT convert(nvarchar(10),getdate(),104) /* 21.10.2020 */
SELECT convert(nvarchar(10),getdate(),106) /* 21 oct 2020 */

--convert dob from date to datetime
SELECT id, name, CONVERT(datetime, dob, 120) as dob from Person

SELECT GETDATE()  /* 2021-02-10 06:23:06.170 */

select Cast('7/17/2011' as datetime) /* 2011-07-17 00:00:00.000 */

