In servlet:
add cookie:
String name, value;
name = "cookie1"; value = "123";
Cookie x = new Cookie(name, value);       
response.addCookie(x);

read cookies:
Cookie [] a = request.getCookies();
if (a == null) {
   out.println("No cookie found");
  } 
 else {
   String name, value;     
   for (Cookie x : a) {
      name = x.getName();
      value = x.getValue();
      out.println("<br>" + name + " = " + value);
    }
  }

Delete all cookies:
Cookie[] a = request.getCookies();
if (a != null) {
     for (Cookie x : a) {
         x.setMaxAge(0);
         response.addCookie(x);
    }
  }
