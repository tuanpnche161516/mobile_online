USE [master]
GO

IF EXISTS (SELECT * FROM sys.databases WHERE name = 'StudentGradeDB')
	DROP DATABASE StudentGradeDB
GO

-- Create the StudentGradeDB database.
CREATE DATABASE StudentGradeDB
GO

USE StudentGradeDB;
GO

-- Create table Classes
CREATE TABLE Classes
 (cl_id varchar (10) PRIMARY KEY,
  cl_name nvarchar(30),
 )
GO

-- Create table Student
CREATE TABLE Student
(id varchar (10) PRIMARY KEY,
 name varchar(40),
 gender bit,
 dob Date,
 cl_id varchar(10)
)
GO

CREATE TABLE Course
 (cr_id varchar (10) PRIMARY KEY,
  cr_name nvarchar(30),
 )
GO

-- Create table Grade
CREATE TABLE Grade
 (id varchar (10),
  cr_id varchar (10),
  mark float,
  PRIMARY KEY (id, cr_id) 
 )
GO

alter table Student add FOREIGN KEY (cl_id) references Classes(cl_id)
GO
alter table Grade add FOREIGN KEY (id) references Student(id)
GO
alter table Grade add FOREIGN KEY (cr_id) references Course(cr_id)
GO

-- Insert data into the Classes table.
INSERT INTO Classes VALUES ('SE11','Software Engineering 11')
INSERT INTO Classes VALUES ('SE12', 'Software Engineering 12')
INSERT INTO Classes VALUES ('CS13', 'Computer Science 13')
INSERT INTO Classes VALUES ('CS14', 'Computer Science 14')
GO

-- Insert data into the Course table.
INSERT INTO Course VALUES ('JAV', 'Java Language')
INSERT INTO Course VALUES ('ENG', 'English')
INSERT INTO Course VALUES ('MAT', 'Mathematics')
INSERT INTO Course VALUES ('CHI', 'Chinese')
GO

-- Insert data into the Student table.
INSERT INTO Student (id, name, gender, dob,cl_id) VALUES ('A1', 'Hoa', 1, '1997-03-17','SE11')
INSERT INTO Student (id, name, gender, dob,cl_id) VALUES ('A3', 'La', 0, '1997-03-17','CS13')
INSERT INTO Student (id, name, gender, dob,cl_id) VALUES ('A2', 'Canh', 1, '1995-04-20','SE12')
INSERT INTO Student (id, name, gender, dob,cl_id) VALUES ('A4', 'Cay', 1, '1996-03-18','SE11')
INSERT INTO Student (id, name, gender, dob,cl_id) VALUES ('A6', 'Goc', 0, '1998-06-19','CS13')
INSERT INTO Student (id, name, gender, dob,cl_id) VALUES ('A5', 'Re', 0, '1999-03-21','CS14')
GO


-- Insert data into the Grade table.
INSERT INTO Grade values ('A2', 'JAV', 7)
INSERT INTO Grade values ('A2', 'ENG', 8)
INSERT INTO Grade values ('A2', 'MAT', 7.5)

Select * from Classes
GO

Select * from Student
GO

Select * from Course
GO

Select * from Grade
GO
