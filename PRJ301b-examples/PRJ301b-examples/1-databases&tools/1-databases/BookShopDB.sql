﻿USE [master]
GO

IF EXISTS (SELECT * FROM sys.databases WHERE name = 'BookShopDB')
	DROP DATABASE BookShopDB
GO

CREATE DATABASE BookShopDB
GO

use BookShopDB
GO


-- create tables
CREATE TABLE Categories (
  id INT IDENTITY (1, 1) PRIMARY KEY,
  name VARCHAR (255) NOT NULL
);
GO


CREATE TABLE Books (
  id INT IDENTITY (1, 1) PRIMARY KEY,
  title VARCHAR (50) NOT NULL,
  cat_id INT NOT NULL,
  price INT NOT NULL,
  FOREIGN KEY (cat_id) REFERENCES categories (id) ON DELETE CASCADE ON UPDATE CASCADE,
);


Create Table Users (
  id INT IDENTITY (1,1) PRIMARY KEY,
  fullname nvarchar(50),
  username nvarchar(50) not null unique,
  password nvarchar(50) not null,
  role int
)
GO

CREATE TABLE Orders (
  id INT IDENTITY (1, 1) PRIMARY KEY,
  user_id INT,
  status tinyint NOT NULL,
  -- Order status: 1 = Pending; 2 = Processing; 3 = Rejected; 4 = Completed
  date DATE not null,
  FOREIGN KEY (user_id) REFERENCES Users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Items (
  ord_id INT,
  book_id INT,   
  quantity INT NOT NULL,
  price INT NOT NULL,
  PRIMARY KEY (ord_id, book_id),
  FOREIGN KEY (ord_id) REFERENCES Orders (id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (book_id) REFERENCES Books (id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Categories table
INSERT INTO Categories (name) VALUES ('Action')
INSERT INTO Categories (name) VALUES ('Comic')
INSERT INTO Categories (name) VALUES ('Mystery')
INSERT INTO Categories (name) VALUES ('Fiction')
GO

-- Books table
INSERT INTO Books (title, cat_id, price) VALUES ('Trek',4,37)
INSERT INTO Books (title, cat_id, price) VALUES ('Ritchey',1,74)
INSERT INTO Books (title, cat_id, price) VALUES ('Surly',2,99)
INSERT INTO Books (title, cat_id, price) VALUES ('Trek',3,28)
INSERT INTO Books (title, cat_id, price) VALUES ('Heller',1,13)
INSERT INTO Books (title, cat_id, price) VALUES ('Cream',2,46)
INSERT INTO Books (title, cat_id, price) VALUES ('Slash',1,39)
INSERT INTO Books (title, cat_id, price) VALUES ('Remedy',4,17)
INSERT INTO Books (title, cat_id, price) VALUES ('Conduit',3,29)
GO

SET IDENTITY_INSERT Users OFF;  
INSERT INTO Users (fullname, username, password, role) VALUES ('Quan','admin','1234',1)
INSERT INTO Users (fullname, username, password, role) VALUES ('Lan','hoa','123',2)
INSERT INTO Users (fullname, username, password, role) VALUES ('Thu','la','12',3)
GO

SET IDENTITY_INSERT Orders OFF;  
INSERT INTO Orders (user_id, status, date) VALUES (1,4,'20160103');
INSERT INTO Orders (user_id, status, date) VALUES (2,4,'20160103');
INSERT INTO Orders (user_id, status, date) VALUES (3,4,'20160103');
INSERT INTO Orders (user_id, status, date) VALUES (1,4,'20160415');
INSERT INTO Orders (user_id, status, date) VALUES (2,4,'20160106');
INSERT INTO Orders (user_id, status, date) VALUES (3,4,'20160105');
INSERT INTO Orders (user_id, status, date) VALUES (1,4,'20160105');
INSERT INTO Orders (user_id, status, date) VALUES (2,4,'20160105');
INSERT INTO Orders (user_id, status, date) VALUES (3,4,'20160108');
INSERT INTO Orders (user_id, status, date) VALUES (1,4,'20160216');
INSERT INTO Orders (user_id, status, date) VALUES (2,4,'20160117');
GO

-- Insert into Items
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (1,1,9,59);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (1,2,8,17);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (1,3,9,15);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (1,4,6,59);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (1,5,4,28);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (2,1,7,59);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (2,2,6,50);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (3,1,3,93);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (3,2,8,59);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (4,1,2,74);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (5,1,1,15);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (5,2,2,42);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (5,3,6,59);
INSERT INTO Items (ord_id,book_id, quantity, price) VALUES (6,1,8,44);
GO

select * from categories;
select * from Users;
select * from Books;
select * from Orders;
select * from Items;
GO
