USE [master]
GO

IF EXISTS (SELECT * FROM sys.databases WHERE name = 'TestDataDB')
	DROP DATABASE TestDataDB
GO

-- Create the TestDataDB database.
CREATE DATABASE TestDataDB
GO

USE TestDataDB;
GO

-- Create table Province
CREATE TABLE Province
 (pro_id varchar (10) PRIMARY KEY,
  pro_name nvarchar(30),
 )
GO

-- Create table Worker
CREATE TABLE Worker
(id varchar (10) PRIMARY KEY,
 name varchar(40),
 gender bit,
 dob Date,
 pro_id varchar(10),
 income int
)
GO

alter table Worker add FOREIGN KEY (pro_id) references Province(pro_id)
GO

-- Insert data into the Province table.
INSERT INTO Province VALUES ('HN','Ha Noi')
INSERT INTO Province VALUES ('HP', 'Hai Phong')
INSERT INTO Province VALUES ('QN', 'Quang Ninh')
INSERT INTO Province VALUES ('VP', 'Vinh Phuc')
GO

-- Insert data into the Worker table.
-- default date: 
--set DATEFORMAT YMD
--GO
INSERT INTO Worker VALUES ('A1', 'Hoa', 1, '1997-03-17','HN',200)
INSERT INTO Worker VALUES ('A3', 'La', 0, '1997-03-17','QN',150)
GO

set DATEFORMAT DMY
GO
INSERT INTO Worker VALUES ('A2', 'Canh', 1, '20-04-1995','HN',300)
INSERT INTO Worker VALUES ('A4', 'Cay', 1, '18-03-1996','HP',400)
GO

set DATEFORMAT MDY
GO
INSERT INTO Worker VALUES ('A6', 'Goc', 0, '06-19-1998','VP',250)
INSERT INTO Worker VALUES ('A5', 'Re', 0, '03-21-1999','HN',350)
GO

Select * from Province
GO

Select * from Worker
GO

select u.id,u.name,u.gender,u.dob,u.pro_id, v.pro_name, u.income from Worker as u, Province as v where u.pro_id = v.pro_id
GO

