USE [master]
GO

IF EXISTS (SELECT * FROM sys.databases WHERE name = 'EmployeeDB')
	DROP DATABASE EmployeeDB
GO

-- Create the EmployeeDB database.
CREATE DATABASE EmployeeDB
GO

USE EmployeeDB;
GO

-- Create table Department
CREATE TABLE Department
 (dep_id varchar (10) PRIMARY KEY,
  dep_name nvarchar(30),
 )
GO

-- Create table Employee
CREATE TABLE Employee
(id varchar (10) PRIMARY KEY,
 name varchar(40),
 gender bit,
 dob Date,
 dep_id varchar(10)
)
GO

alter table Employee add FOREIGN KEY (dep_id) references Department(dep_id)
GO

-- Insert data into the Department table.
INSERT INTO Department VALUES ('MA','Marketing')
INSERT INTO Department VALUES ('SA', 'Sale')
INSERT INTO Department VALUES ('AC', 'Accounting')
INSERT INTO Department VALUES ('DE', 'Development')
GO

-- Insert data into the Employee table.
INSERT INTO Employee (id, name, gender, dob,dep_id) VALUES ('A1', 'Hoa', 1, '1997-03-17','MA')
INSERT INTO Employee (id, name, gender, dob,dep_id) VALUES ('A3', 'La', 0, '1997-03-17','AC')
INSERT INTO Employee (id, name, gender, dob,dep_id) VALUES ('A2', 'Canh', 1, '1995-04-20','MA')
INSERT INTO Employee (id, name, gender, dob,dep_id) VALUES ('A4', 'Cay', 1, '1996-03-18','SA')
INSERT INTO Employee (id, name, gender, dob,dep_id) VALUES ('A6', 'Goc', 0, '1998-06-19','DE')
INSERT INTO Employee (id, name, gender, dob,dep_id) VALUES ('A5', 'Re', 0, '1999-03-21','MA')
GO

Select * from Department
GO

Select * from Employee
GO
