USE [master]
GO

IF EXISTS (SELECT * FROM sys.databases WHERE name = 'RosterDB')
	DROP DATABASE RosterDB
GO

-- Create database RosterDB
create database RosterDB character set utf8
GO

-- Create tables
DROP TABLE IF EXISTS Member;
DROP TABLE IF EXISTS `User`;
DROP TABLE IF EXISTS Course;

CREATE TABLE `User` (
    user_id     INTEGER NOT NULL AUTO_INCREMENT,
    name        VARCHAR(128) UNIQUE,
    PRIMARY KEY(user_id)
) ENGINE=InnoDB CHARACTER SET=utf8;

CREATE TABLE Course (
    course_id     INTEGER NOT NULL AUTO_INCREMENT,
    title         VARCHAR(128) UNIQUE,
    PRIMARY KEY(course_id)
) ENGINE=InnoDB CHARACTER SET=utf8;

CREATE TABLE Member (
    user_id       INTEGER,
    course_id     INTEGER,
    role          INTEGER,

    CONSTRAINT FOREIGN KEY (user_id) REFERENCES `User` (user_id)
      ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FOREIGN KEY (course_id) REFERENCES Course (course_id)
       ON DELETE CASCADE ON UPDATE CASCADE,

    PRIMARY KEY (user_id, course_id)
) ENGINE=InnoDB CHARACTER SET=utf8;

-- insert data into user
insert into user (name) values ('Juniper');
insert into user (name) values ('Arandeep');
insert into user (name) values ('Orin');
insert into user (name) values ('Tymoteusz');
insert into user (name) values ('Yousif');
insert into user (name) values ('Pele');
insert into user (name) values ('Ammaar');
insert into user (name) values ('Marty');
insert into user (name) values ('Roisin');
insert into user (name) values ('William');
insert into user (name) values ('Gigha');
insert into user (name) values ('Cheyanne');
insert into user (name) values ('Derren');
insert into user (name) values ('Eugene');
insert into user (name) values ('Tessa');

-- insert data into course
insert into course (title) values ('si106');
insert into course (title) values ('si110');
insert into course (title) values ('si206');

-- insert data into member
INSERT INTO member (user_id, course_id, role) VALUES (1,1,1);
INSERT INTO member (user_id, course_id, role) VALUES (2,1,0);
INSERT INTO member (user_id, course_id, role) VALUES (3,1,0);
INSERT INTO member (user_id, course_id, role) VALUES (4,1,0);
INSERT INTO member (user_id, course_id, role) VALUES (5,1,0);
INSERT INTO member (user_id, course_id, role) VALUES (6,2,1);
INSERT INTO member (user_id, course_id, role) VALUES (7,2,0);
INSERT INTO member (user_id, course_id, role) VALUES (8,2,0);
INSERT INTO member (user_id, course_id, role) VALUES (9,2,0);
INSERT INTO member (user_id, course_id, role) VALUES (10,2,0);
INSERT INTO member (user_id, course_id, role) VALUES (11,3,1);
INSERT INTO member (user_id, course_id, role) VALUES (12,3,0);
INSERT INTO member (user_id, course_id, role) VALUES (13,3,0);
INSERT INTO member (user_id, course_id, role) VALUES (14,3,0);
INSERT INTO member (user_id, course_id, role) VALUES (15,3,0);

SELECT User.name, Course.title, Member.role
    FROM User JOIN Member JOIN Course
    ON User.user_id = Member.user_id AND Member.course_id = Course.course_id
    ORDER BY Course.title, Member.role DESC, User.name
