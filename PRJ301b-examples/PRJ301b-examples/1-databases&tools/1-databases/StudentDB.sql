﻿CREATE DATABASE StudentDB2
GO

use StudentDB2
GO
Create Table Student (RollNo varchar(10),Name varchar(30),mark float, AB int , PRIMARY KEY (RollNo))
GO
INSERT INTO Student (RollNo,Name,AB,Mark) VALUES ('A1','Le Man', 5, 9)
INSERT INTO Student (RollNo,Name,AB,Mark) VALUES ('A2','Tran Dao', 9)
INSERT INTO Student (RollNo,Name,AB,Mark) VALUES ('B1','Dang Xuan', 8, 7)
INSERT INTO Student (RollNo,Name,AB,Mark) VALUES ('B2','Phan Ha', 7, 6)
INSERT INTO Student (RollNo,Name,AB,Mark) VALUES ('B3','Hoang Thu', 8, 5)
INSERT INTO Student (RollNo,Name,AB,Mark) VALUES ('B4','Nguyen Dong', 7,4)
INSERT INTO Student (RollNo,Name,AB,Mark) VALUES ('C2','Pham Thu', 7, 3)
GO
select * from student
GO
drop DATABASE StudentDB2